import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {
	public static void main(String[] args) 
			throws UnknownHostException, IOException {
		Socket so = new Socket("127.0.0.1", 9001);
		BufferedWriter out = new BufferedWriter(
				new OutputStreamWriter(so.getOutputStream()));
		BufferedReader in = new BufferedReader(
				new InputStreamReader(System.in)
		);
		String message = "";
		System.out.print("Please enter action: ");
		try {
			message = in.readLine();
		}
		catch (IOException e){
			e.printStackTrace();
		}
		System.out.println("Action: " + message);
		out.write(message, 0, message.length());
		out.newLine();
		out.flush();
		so.close();
		out.close();
	}
}
