import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

	private static void exit() {
		System.out.println("Closing now!");
	}

	private static void clean() {
		System.out.println("Starting cleaning now!");
	}

	private static void stop() {
		System.out.println("Robot stopped!");
	}

	private static void backToDock() {
		System.out.println("Returning to dock!");
	}

	public static void main(String[] args) throws IOException {
		System.out.println("Server has started...");
		ServerSocket s = new ServerSocket(9001);
		var serverActive = true;
		while(serverActive) {
			Socket so = s.accept();

			BufferedReader in = new BufferedReader(
					new InputStreamReader(so.getInputStream()));
			String message = in.readLine();
			switch (message) {
				case "close":
					exit();
					serverActive = false;
					break;
				case "clean":
					clean();
					break;
				case "stop":
					stop();
					break;
				case "back to dock":
					backToDock();
					break;
				default:
					System.out.println(String.format("No such action: %s", message));
			}
			so.close();
		}
		s.close();
	}
}
