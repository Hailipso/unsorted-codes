#ifndef ORDER_H
#define ORDER_H

#include <QObject>
#include <QDebug>

class Order : public QObject
{
    Q_OBJECT
public:
    Order(int _id, double _totalPrice,QString _customerName, bool _hasGifts);
    void Print();
    int id;
    double totalPrice;
    QString customerName;
    bool hasGifts;
};

#endif // ORDER_H
