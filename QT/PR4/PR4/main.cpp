#include <QCoreApplication>
#include <order.h>
#include <QFileDialog>
#include <QFile>
#include <QSaveFile>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QStandardPaths>
#include <QDebug>
// Romanov Stanislav Gruppa BBBO-02-18, Variant 3 (JSON)
void Writing(){
    qDebug() << "Writing:";
    QList<Order *>* writing = new QList<Order *>;
    Order *first = new Order(1,5.0,"Ivanov", false);
    Order *second = new Order(2,10.0,"Bogdanoff", true);
    writing->append(first);
    writing->append(second);
    for (Order *order : *writing){
        order->Print();
    }
    QJsonArray* jarray = new QJsonArray;
    for (Order *order : *writing){
        QJsonObject result;
        result.insert("id",order->id);
        result.insert("totalPrice",order->totalPrice);
        result.insert("customerName",order->customerName);
        result.insert("hasGifts",order->hasGifts);
        jarray->append(result);
    }
    QJsonDocument jdoc(*jarray);
    QString fileName = QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).first() + "/PR4test.json";
    QFile file(fileName);
    if (file.open(QFile::WriteOnly | QFile::Text)){
    QTextStream stream(&file);
    stream << jdoc.toJson();
    file.close();
    }
}

void Reading(){
    qDebug() << "Reading:";
    QList<Order *>* reading = new QList<Order *>;
    QString fileName = QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).first() + "/PR4test.json";
    QFile file(fileName);
    if (file.open(QFile::ReadOnly | QFile::Text)){
        QTextStream stream(&file);
        QJsonDocument jdoc = QJsonDocument::fromJson(stream.readAll().toLocal8Bit());
        QJsonArray jarray = jdoc.array();
        for (QJsonArray::Iterator i = jarray.begin(); i != jarray.end(); i++){
            Order* object = new Order(
                        i->toObject().value("id").toInt(),
                        i->toObject().value("totalPrice").toDouble(),
                        i->toObject().value("customerName").toString(),
                        i->toObject().value("hasGifts").toBool());
            reading->append(object);
        }
    }
    for (Order *order : *reading){
        order->Print();
    }
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    Writing();
    Reading();
    return a.exec();
}
