#include <QCoreApplication>
#include <QDebug>
#include <QFile>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>
#include <QStandardPaths>

class Order
{
    public:
    int id;
    double totalPrise;
    QString customerName;
    bool hasGifts;
    Order(int i, double Prise, QString Name, bool has)
    {
        id = i;
        totalPrise = Prise;
        customerName = Name;
        hasGifts = has;
    }
    void print()
    {
        qDebug() << id << Qt::endl << totalPrise << Qt::endl << customerName << Qt::endl << hasGifts;
    }
};

void writing (QFile *file, QList<Order*> list) {
    if(file->open(QFile::WriteOnly))
    {
         QXmlStreamWriter stream(file);
          stream.setAutoFormatting(true);
         stream.writeStartDocument();
         stream.writeStartElement("Praktika5");
         for(int i = 0; i < list.length(); i++){
             QString name = QString("order%1").arg(i);
             stream.writeStartElement(name);
             QString text = QString("%1").arg(list[i]->id);
             stream.writeTextElement("id",text);
             text = QString("%1").arg(list[i]->totalPrise);
             stream.writeTextElement("totalPrise",text);
             text = QString("%1").arg(list[i]->customerName);
             stream.writeTextElement("customerName",text);
             text = QString("%1").arg(list[i]->hasGifts);
             stream.writeTextElement("hasGifts",text);
             stream.writeEndElement();
         }
         stream.writeEndElement();
         stream.writeEndDocument();
         file->close();
    }
    else
        qDebug() << "Ошибка открытия файла";
}

QList<Order*> reading (QFile *file){
    QList<Order*> retList;
    int id;
    double totalPrise;
    QString customerName;
    bool hasGifts;
    if(file->open(QFile::ReadOnly))
    {
        QXmlStreamReader stream(file);
        stream.readNext();
        if (stream.isStartDocument()){
            while (!stream.atEnd() && !stream.hasError()){
                stream.readNext();
                auto token = stream.tokenType();
                auto tokenstring = stream.tokenString();
                auto text = stream.text();
                if (stream.isStartElement()){
                    if (stream.name() == "id"){
                        id = stream.text().toInt();
                        qDebug() << stream.text();
                        qDebug() << id;
                    }
                }
            }
        }
    }
    else
        qDebug() << "Ошибка открытия файла";
    file->close();
    return retList;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    Order *order1 = new Order(0, 18, "Alexey", false);
    Order *order2 = new Order(1, 42, "Andrey", false);
    Order *order3 = new Order(2, 7, "Afanasiy", true);
    QList<Order*> list1;
    list1.append(order1);
    list1.append(order2);
    list1.append(order3);
    for(int i = 0; i < list1.length(); i++)
    list1[i]->print();

    QString filename = QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).first() +"/file.xml";
    QFile *file_XML = new QFile(filename);
    writing (file_XML, list1);
    QList<Order*> List2 = reading (file_XML);
}
