#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QPainter>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    QPainter *painter = new QPainter(this);
    painter->setRenderHint(QPainter::Antialiasing);
    painter->drawText(QPointF(5,15),"Студент: Станислав Романов");
    painter->drawText(QPointF(5,29),"Группа: БББО-02-18");
    painter->drawText(QPointF(5,43),"Рисунок выполнен средствами QPainter");
    //rect
    QRectF* rect = new QRectF(QPointF(300,100),QSize(100,60));
    painter->drawRect(*rect);
    painter->drawText(*rect,Qt::AlignCenter,"Корень");
    //from rect to poly
    painter->drawLine(300,130,200,130);
    painter->drawLine(200,130,200,200);
    painter->drawLine(400,130,500,130);
    painter->drawLine(500,130,500,200);
    //poly
    QPolygon* poly = new QPolygon();
    *poly << QPoint(150,230) << QPoint(175,200) << QPoint(225,200)
          << QPoint(250,230) << QPoint(225,260) << QPoint(175,260);
    painter->drawPolygon(*poly);
    painter->drawText(poly->boundingRect(),Qt::AlignCenter,"1.2");
    for (int i = 0; i<6;i++){
        QPoint point(poly->at(i).x() + 300, poly->at(i).y());
        poly->setPoint(i,point);
    }
    painter->drawPolygon(*poly);
    painter->drawText(poly->boundingRect(),Qt::AlignCenter,"1.3");
    //from poly to circle
    for (int i=0; i<2;i++)
    {
        painter->drawLine(150+i*300,230,100+i*300,230);
        painter->drawLine(100+i*300,230,100+i*300,300);
        painter->drawLine(250+i*300,230,300+i*300,230);
        painter->drawLine(300+i*300,230,300+i*300,300);
    }
    //circle
    for (int i=0;i<2;i++)
    {
        for (int j=0;j<=1;j++)
        {
            QRectF* circle = new QRectF(QPoint(70+j*200+i*300,300),QSize(60,60));
            painter->drawEllipse(*circle);
            painter->drawText(*circle,Qt::AlignCenter,QString("1.%1.%2").arg(i+2).arg(j+1));
        }
    }
}
