#pragma once
#include <QObject>
class SignalSlot : public QObject
{
	Q_OBJECT;
public:
	SignalSlot();
public slots:
	void Slot();
signals:
	void Signal();
};

