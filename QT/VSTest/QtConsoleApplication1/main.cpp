#include <QtCore/QCoreApplication>
#include "signal.h"
#include <QObject>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    SignalSlot* signal=new SignalSlot();
    SignalSlot* slot = new SignalSlot();
    QObject::connect(signal, SIGNAL(Signal()), slot, SLOT(Slot()));
    return a.exec();
}
