#include <QCoreApplication>
#include "QDebug"
static int c=0;


class ca
{
private:
 int _a;
public:
 ca(){ _a=5; }
 int geta(){ return _a+c; }
 void seta(int a){ _a=a; }
 ~ca() { _a=2; c=7; }
};
class cb:public ca
{
private:
 int _b;
public:
 cb():ca(){ _b=10; }
 int getb() { return _b+geta(); }
 ~cb(){ c=3; }
};


class cc
{
private:
 ca * _a;
public:
 cc(ca *a1){ _a = a1; }
 int getc(){ return _a->geta()+c; }
};


int main(int argc, char *argv[])
{
 QCoreApplication a(argc, argv);

 ca * a2 = new ca(); // a2->_a=5
 cb * b1 = new cb(); // b1->_a=5, b1->b=10
 if(true)
 {
 ca a1;             //_a=5
 qDebug()<<a1.geta()+c; //5
 }                  //c=7
 qDebug()<<b1->getb();  //10+5+7=22
 cc c1(a2);
 a2->seta(8);
 delete b1;
 qDebug()<<c1.getc();   //8+7+7=22
 delete a2;
 return a.exec();
}

