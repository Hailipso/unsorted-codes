#include "datamodel.h"
//Поменять в columnCount() [22 строка] 6 на количество столбцов
//и в headerData() [с 91 строки] вставить названия столбцов

DataModel::DataModel(QObject *parent) : QAbstractTableModel(parent)
{
    modelList = new QVector<Data*>();

}

int DataModel::rowCount(const QModelIndex &parent) const
{
    return modelList->size();
}

int DataModel::rowCount() const
{
    return modelList->size();
}

int DataModel::columnCount(const QModelIndex &parent) const
{
    return 6;
}

QVariant DataModel::data(const QModelIndex &index, int role) const
{
    if (role == Qt::DisplayRole || role == Qt::EditRole)
    {
        int i = index.row();
        int j = index.column();
        return this->modelList->at(i)->GetProperty(j);
    }
    else return QVariant();
}

bool DataModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    modelList->at(index.row())->SetProperty(index.column(),value.toString());
    emit dataChanged(index,index);
    return true;
}

Qt::ItemFlags DataModel::flags(const QModelIndex &index) const
{
    return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable;
}

void DataModel::clear(){
    beginResetModel();
    delete modelList;
    modelList = new QVector<Data*>;
    endResetModel();
}

bool DataModel::insertRows(int row, int count, const QModelIndex &parent)
{
    beginInsertRows(parent,row,row+count-1);
    modelList->append(new Data);
    endInsertRows();
    return true;
}

bool DataModel::insertRows(int row, int count, Data *object, const QModelIndex &parent)
{
    beginInsertRows(parent,row,row+count-1);
    modelList->append(object);
    endInsertRows();
    return true;
}

bool DataModel::removeRows(int row, int count, const QModelIndex &parent)
{
    if (row < 0 || row > modelList->count()) return false;
    beginRemoveRows(parent,row,row+count-1);
    for (int i = 0; i < count; ++i){
        modelList->remove(row);}
    endRemoveRows();
    return true;
}

QVector<Data *>* DataModel::getVector()
{
    return modelList;
}


QVariant DataModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Horizontal)
        switch (section){
            case 0: return "ObjectID";
            case 1: return "Region";
            case 2: return "Type";
            case 3: return "Safety Rating";
            case 4: return "Owner";
            case 5: return "Personal Data";
        }
    return QVariant();
}
