#include "data.h"
#include <QObject>
#include <QString>
#include <QJsonObject>
/*Данные под свою таблицу, по сути в каждой функции менять переменные на свои
всё, кроме идентификатора легче держать в QString;
не забыть поменять/добавить/удалить переменные в data.h*/

Data::Data()
{
    ObjectID = 0;
    Region = "";
    Type = "";
    SafetyRating = "";
    Owner = "";
    PersonalData = "";
}

Data::Data(const QJsonObject &object)
{
    ObjectID = object.value("ObjectID").toInt();
    Region = object.value("Region").toString();
    Type = object.value("Type").toString();
    SafetyRating = object.value("Safety Rating").toString();
    Owner = object.value("Owner").toString();
    PersonalData = object.value("Personal Data").toString();
}


QJsonObject Data::ToJson()
{
    QJsonObject result;
    result.insert("ObjectID", ObjectID);
    result.insert("Region", Region);
    result.insert("Type", Type);
    result.insert("Safety Rating", SafetyRating);
    result.insert("Owner", Owner);
    result.insert("Personal Data",PersonalData);
    return result;
}

QString Data::GetProperty(int PropID) const
{
    switch(PropID){
        case(0): return QString::number(ObjectID); break;
        case(1): return Region; break;
        case(2): return Type; break;
        case(3): return SafetyRating; break;
        case(4): return Owner; break;
        case(5): return PersonalData; break;
    default: return "";
    }
}

void Data::SetProperty(int PropID, QString property)
{
    switch(PropID)
    {
        case(0): ObjectID = property.toInt(); break;
        case(1): Region = property; break;
        case(2): Type = property; break;
        case(3): SafetyRating = property; break;
        case(4): Owner = property; break;
        case(5): PersonalData = property; break;
        default: return;
    }
}
