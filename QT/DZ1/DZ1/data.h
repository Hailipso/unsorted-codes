#ifndef DATA_H
#define DATA_H

#include <QObject>

class Data
{
    int ObjectID;
    QString Region, Type, SafetyRating, Owner, PersonalData;
public:
    Data();
    Data(const QJsonObject &object);
    QJsonObject ToJson();
    QString GetProperty(int) const;
    void SetProperty(int,QString);
};

#endif // DATA_H
