#include "data.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QDataStream>
#include <QFile>
#include <QTextStream>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
/*Мои старые пояснения на английском, в отличии от моих других комментариев,
 можно их оставить/перевести(желательно), чтобы легче защитить код*/

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
//create view, container, model
    ui->setupUi(this);
    model = new DataModel();
//prepare view
    ui->tableView->setModel(model);
    ui->tableView->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
//connect
    connect(ui->OpenFileBt,SIGNAL(clicked()),this,SLOT(OpenFile()));
    connect(ui->SaveFileBt,SIGNAL(clicked()),this,SLOT(SaveFile()));
    connect(ui->InsertRowBt,SIGNAL(clicked()),this,SLOT(InsertRow()));
    connect(ui->RemoveRowBt,SIGNAL(clicked()),this,SLOT(DeleteRow()));
}

MainWindow::~MainWindow()
{
    delete ui;
    delete model;
}

void MainWindow::OpenFile()
{
//Open File
    QString FileName = QFileDialog::getOpenFileName(this, "Open JSON", "", "Json (*.JSON)");
    if (FileName.isNull()) {qDebug() << "Open Error : File not found"; return;}
    QFile file(FileName);
    if (!file.open(QFile::ReadOnly | QFile::Text))
    {
        qDebug() << "Open error : File could not be opened.";
        return;
    }
    QTextStream stream(&file);
//Parse JSON to JsonDoc
    QJsonDocument jsondoc = QJsonDocument::fromJson(stream.readAll().toLocal8Bit());
    file.close();
    if (jsondoc.isEmpty())
    {
        qDebug()<< "Parse error : JSON empty or file cannot be parsed";
        return;
    }
//Clear old data
    model->clear();
//Parse JsonDoc to Objects and Fill Data
    if  (jsondoc.isObject())
    {
        Data *object = new Data(jsondoc.object());
        model->insertRows(0,1,object);
    }
    else
    {
        QJsonArray jarray = jsondoc.array();
        for (QJsonArray::Iterator i = jarray.begin(); i != jarray.end(); i++)
        {
            Data *object = new Data(i->toObject());
            model->insertRows(i.i,1,object);
        }
    }
}

void MainWindow::SaveFile()
{
//prepare json
    QJsonArray jarray;
    QVector<Data*> *vector = model->getVector();
    foreach (Data* DataObject, *vector)
    {
        jarray.append(DataObject->ToJson());
    }
    QJsonDocument jsondoc(jarray);
//save to file
    QString FileName = QFileDialog::getSaveFileName(this, "Save JSON", "", "Json (*.JSON)");
    QFile file(FileName);
    if (file.open(QFile::WriteOnly | QFile::Text))
    {
        QTextStream stream(&file);
        stream << jsondoc.toJson();
        file.close();
    }
    else qDebug() << "Save error : File cannot be opened/created.";
}

void MainWindow::InsertRow()
{
    model->insertRows(model->rowCount(),1);
}

void MainWindow::DeleteRow(){
    QModelIndexList indexList = ui->tableView->selectionModel()->selectedIndexes();
    if (indexList.count() != 0)
        model->removeRows(indexList.first().row(),indexList.count());
}
