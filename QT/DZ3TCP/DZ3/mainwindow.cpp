#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QInputDialog>
#include <QHostAddress>
#include <QRegularExpression>
#include <QNetworkDatagram>
#include <QTextStream>
#include <QDateTime>
#include <QSqlQuery>
#include <QSqlError>
#include <QtGlobal>
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    //setup sockets
    ListeningTCP = new QTcpServer();
    RecievingTCP = new QTcpSocket();
    IP = new QHostAddress(QHostAddress::LocalHost);
    ui->setupUi(this);
    //set connections
    connect(ui->actionListeningPort,&QAction::triggered,
            this,&MainWindow::SetListeningPort);
    connect(ui->actionRecievingPort,&QAction::triggered,
            this,&MainWindow::SetRecieveingPort);
    connect(ui->actionRecievingIP,&QAction::triggered,
            this,&MainWindow::SetIP);
    connect(ui->SendBt,&QPushButton::clicked,
            this,&MainWindow::SendMessage);
    connect(ListeningTCP,&QTcpServer::newConnection,
            this,&MainWindow::ConnectClient);
    //setup ui
    ui->SendBt->setDisabled(true);
    //connect to database
    db.setDatabaseName("log.db");
    if (!db.open()) qDebug() << db.lastError();
    //create table if none exists
    QSqlQuery query;
    query.exec("CREATE TABLE IF NOT EXISTS Logs ( "
    "ID INTEGER PRIMARY KEY AUTOINCREMENT , "
    "type VARCHAR(8) NOT NULL , "
    "datetime DATETIME NOT NULL , "
    "filename TEXT NOT NULL )");
    //get history
    if (query.exec("SELECT type, datetime, filename FROM Logs"))
        while (query.next())
        {
            if (query.value(0).toString().contains("sent"))
                ui->textEdit->append(QString(query.value(1).toString()+
                                             " File sent: "+
                                             query.value(2).toString()));
            else ui->textEdit->append(QString(query.value(1).toString()+
                                              " File recieved: "+
                                              query.value(2).toString()));
        }
    ui->textEdit->append("\n\nNotice: Please setup ports to begin\n");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::SetListeningPort()
{
    bool ok = false;
    ListeningPort = QInputDialog::getInt(this,"Enter Listening Port", "Port",1,1,65355,1, &ok);
    if (ok)
    {
        ListeningTCP->close();
        ListeningTCP->listen(QHostAddress::Any,ListeningPort);
        ui->textEdit->append("Listening on port " + QString::number(ListeningPort));
    }
}

void MainWindow::SetRecieveingPort()
{
    bool ok = false;
    RecievingPort = QInputDialog::getInt(this,"Enter Recieving Port", "Port",1,1,65355,1,&ok);
    RecievingTCP->connectToHost(*IP,RecievingPort,QIODevice::ReadWrite);
    if (ok){
        ui->textEdit->append("Sending to " + IP->toString() + ":" + QString::number(RecievingPort));
        ui->SendBt->setEnabled(true);
    }
    connect(RecievingTCP,&QTcpSocket::readyRead,
            this, [=]() {onReadyRead(RecievingTCP);});
}

void MainWindow::SetIP()
{
    bool ok = false;
    QString _ipText = QInputDialog::getText(this,"Enter Recieving IPv4", "IP",QLineEdit::Normal,QString(),&ok);
    QRegularExpression regex("^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$");
    QRegularExpressionMatch match = regex.match(_ipText);
    if (ok && match.hasMatch())
    {
        IP = new QHostAddress(_ipText);
        ui->textEdit->append("New target IP: " + IP->toString());
    }
    else if (ok) ui->textEdit->append("Error: Invalid IP");
}

void MainWindow::SendMessage()
{
    filename = QFileDialog::getOpenFileName(this,"Choose file to send","","Any (*.*)");
    file.setFileName(filename);
    if (!file.open(QIODevice::ReadOnly))
    {
        ui->textEdit->append("File could not be opened: " + filename);
        return;
    }
    QString text = "Sending file " + filename;
    QString time = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
    QString message = time + "   >   " + text;
    QDataStream out(&file), meta(RecievingTCP);
    meta << QString("SizeIs: " + QString::number(file.size())).toUtf8();
}

void MainWindow::ConnectClient()
{
    while (ListeningTCP->hasPendingConnections())
    {
        QTcpSocket* ReadingTCP = ListeningTCP->nextPendingConnection();
        connect(ReadingTCP, &QTcpSocket::readyRead,
                this, [=]() {onReadyRead(ReadingTCP);});
        connect(ReadingTCP,&QTcpSocket::disconnected,
                ReadingTCP,&QTcpSocket::deleteLater);
    }
}

void MainWindow::onReadyRead(QTcpSocket* ReadingTCP)
{
    QByteArray block;
    QDataStream in(ReadingTCP), out(ReadingTCP);
    in >> block;
    if (QString::fromUtf8(block).contains("SizeIs: ")) //recieved size, answering when ready to start
    {
        fileread = 0;
        size = QString::fromUtf8(block).section(": ", 1, 1).toInt();
        ui->progressBar->setMaximum(size);
        wrfilename = QFileDialog::getSaveFileName
                (this,"Where to save","","Any (*.*)");
        wrfile.setFileName(wrfilename);
        if (!wrfile.open(QIODevice::WriteOnly)) return;
        out << QString("WriteReady! ").toUtf8();
        return;
    }
    else if (QString::fromUtf8(block).contains("WriteReady! ")) //recieved ready to write, send next chunk
    {
        out << file.read(2048);
    }
    else if (QString::fromUtf8(block).contains("WriteEnded! ")) //recieved ending signal, registering file sent
    {
        file.close();
        AddLog(filename,sent);
    }
    else // recieved file chunk, reading and writing to new file.
    {
        fileread+=2048;
        wrfile.write(block);
        ui->progressBar->setValue(fileread);
        if (fileread >= size) // whole file read, send ending signal
        {
            wrfile.close();
            out << QString("WriteEnded! ").toUtf8();
            ui->progressBar->reset();
            AddLog(wrfilename,recieved);
            return;
        }
        out << QString("WriteReady! ").toUtf8();
    }
}

void MainWindow::AddLog(QString filename, MessageType type)
{
    QString time = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
    if (type == sent)
        ui->textEdit->append(time + " File sent: " + filename);
    else
        ui->textEdit->append(time + " File recieved: " + filename);
    QSqlQuery query;
    query.prepare("INSERT INTO Logs (type, datetime, filename) VALUES"
"(:type,:datetime,:filename)");
    if (type == sent) query.bindValue(":type", "sent");
        else query.bindValue(":type", "recieved");
    query.bindValue(":datetime", time);
    query.bindValue(":filename", filename);
    query.exec();
}
