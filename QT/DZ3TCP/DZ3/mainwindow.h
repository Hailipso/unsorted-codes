#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>
#include <QTcpServer>
#include <QSqlDatabase>
#include <QHostAddress>
#include <qfile.h>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    enum MessageType : bool
    {
        sent = true,
        recieved = false
    };

    void AddLog(QString,MessageType);

public slots:
    void SetListeningPort();
    void SetRecieveingPort();
    void SetIP();
    void SendMessage();
    void ConnectClient();
    void onReadyRead(QTcpSocket*);
private:
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    QTcpSocket* RecievingTCP;
    QTcpServer* ListeningTCP;
    QVector<QTcpSocket*>* Connections;
    QHostAddress* IP;
    QFile wrfile, file;
    int size, fileread=0;
    QString wrfilename, filename;
    int ListeningPort, RecievingPort;
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
