#include <QtCore/QCoreApplication>
#include "signalslot.h"
#include <QObject>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
       SignalSlot* signal=new SignalSlot();
       SignalSlot* slot = new SignalSlot();
       QObject::connect(signal, SIGNAL(Signal(QString)), slot, SLOT(Slot(QString)));
       signal->EmitSignal();
    return a.exec();
}
