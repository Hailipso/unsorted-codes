#ifndef SIGNALSLOT_H
#define SIGNALSLOT_H

#include <QObject>

class SignalSlot : public QObject
{
    QString string = "test";
    Q_OBJECT;
public:
    SignalSlot();
    void EmitSignal();
public slots:
    void Slot(QString);
signals:
    void Signal(QString);
};

#endif // SIGNALSLOT_H
