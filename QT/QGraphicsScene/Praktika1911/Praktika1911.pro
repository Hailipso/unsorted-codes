#-------------------------------------------------
#
# Project created by QtCreator 2021-11-19T10:47:14
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Praktika1911
TEMPLATE = app

CONFIG += c++11

SOURCES += main.cpp\
        mainwindow.cpp \
    myitem.cpp

HEADERS  += mainwindow.h \
    myitem.h

FORMS    += mainwindow.ui
