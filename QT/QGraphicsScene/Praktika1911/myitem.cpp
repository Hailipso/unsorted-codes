#include "myitem.h"
#include <QPainter>
#include <QRectF>
#include <QStyleOptionGraphicsItem>
myitem::myitem()
{

}

QRectF myitem::boundingRect() const
{
    return QRectF(0,0,250,250);
}

void myitem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->drawText(100,100, "Hello");
    painter->drawLine(QLine(0,0,150,150));
    painter->drawRect(boundingRect());
}
