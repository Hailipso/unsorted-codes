#ifndef MYITEM_H
#define MYITEM_H

#include <QGraphicsItem>
class myitem : public QGraphicsItem
{
public:
    myitem();
    QRectF boundingRect() const;


    // QGraphicsItem interface
public:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
};

#endif // MYITEM_H
