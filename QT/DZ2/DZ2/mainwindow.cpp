#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFile>
#include <QFileDialog>
#include <QTextStream>
#include <QDebug>
#include <QRegularExpression>
#include "drawingclass.h"
#include "arrow.h"
#include <QPointer>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->graphicsView->setScene(&scene);
    connect(ui->LoadHeaderBt,SIGNAL(clicked()),this,SLOT(LoadFile()));
    classes = new QMap<QString,DrawingClass*>();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::LoadFile()
{
//open file
    QString FileName = QFileDialog::getOpenFileName(this, "Open header", "", "Header (*.h)");
    if (FileName.isNull()) {qDebug() << "Open Error : File not found"; return;}
    QFile file(FileName);
    if (!file.open(QFile::ReadOnly | QFile::Text))
    {
        qDebug() << "Open error : File could not be opened.";
        return;
    }
    classes->clear();
    QTextStream stream(&file);
    QString header_string = stream.readAll();
//parse header file for classes and set height per inheritance level
    QRegularExpression NameStringRegex("(?!\\w*\\s+|)*class\\s+(\\w+)\\s*(?::\\s*(?:\\w*\\s*,?)*)?(?=;|,|\\s*{)");
    QRegularExpression ParentRegex("\\w+(?=(?:\\s*,\\s*)|\\s*(?:\\n|{)|$)");
    QRegularExpressionMatchIterator iterator = NameStringRegex.globalMatch(header_string);
    int min = 0,max = 0;
    while (iterator.hasNext())
    {
        QRegularExpressionMatch match = iterator.next();
        DrawingClass* _class;
        if (!classes->contains(match.captured(1)))
            _class = new DrawingClass(match.captured(1));
        else
            _class = classes->value(match.captured(1));
        QRegularExpressionMatchIterator parentIter = ParentRegex.globalMatch(match.captured());
        QVector <DrawingClass*> parents;
        bool levelChanged = false;
        //check for parents
        while (parentIter.hasNext())
        {
            QRegularExpressionMatch parentMatch = parentIter.next();
            DrawingClass* parent;
            if (!classes->contains(parentMatch.captured()))
            {
                parent = new DrawingClass(_class->GetLevel()+1,parentMatch.captured());
                classes->insert(parent->GetName(),parent);
                _class->parents->append(parent);
            }
            else
            {
                parent = classes->value(parentMatch.captured());
                if (!_class->parents->contains(parent))
                    _class->parents->append(parent);
                //if parent with equal or lower InheritanceLevel exists,
                //set Inheritance level below parent and update InheritenceLevel for children
                if (parent->GetLevel() <= _class->GetLevel())
                {
                    _class->Setlevel(parent->GetLevel()-1);
                    levelChanged = true;
                }
            }
        }
        if (levelChanged) _class->levelUpdated(classes);
        if (!classes->contains(_class->GetName()))
        classes->insert(_class->GetName(),_class);
        else foreach (DrawingClass* parent, parents)
            classes->value(_class->GetName())->parents->append(parent);
    }
    foreach (DrawingClass* _class, *classes)
    {
        if (_class->GetLevel() > max) max = _class->GetLevel();
        if (_class->GetLevel() < min) min = _class->GetLevel();
    }
//draw classes
    scene.clear();
    QVector<int> max_offsets;
    for (int i = max; i >= min ; i--) max_offsets.append(0);
    foreach (DrawingClass* _class, *classes)
    {
        max_offsets.replace(_class->GetLevel()-min,
                            max_offsets.at(_class->GetLevel()-min)+1);
    }
    QVector<int> offsets;
    for (int i = max; i >= min ; i--) offsets.append(0);
    foreach (DrawingClass* _class, *classes)
    {
        int offset = _class->GetLevel()-min;
        _class->setPos(200*offsets.at(offset)-200/2*max_offsets.at(offset),
                       -150*(offset));
        _class->setZValue(0);
        scene.addItem(_class);
        offsets.replace(offset,
                        offsets.at(offset)+1);
    }
//draw connections
    foreach (DrawingClass* _class, *classes)
    {
        foreach (DrawingClass* _parent, *_class->parents)
        {
            Arrow* _arrow = new Arrow (_parent,_class);
            _arrow->setZValue(-1);
            scene.addItem(_arrow);
        }
    }
}
