#include "drawingclass.h"
#include <QPainter>
#include <QGraphicsScene>

DrawingClass::DrawingClass()
{

}

DrawingClass::DrawingClass(QString _name)
{
    InheritanceLevel = 0;
    name = _name;
    parents = new QVector<DrawingClass*>();
}

DrawingClass::DrawingClass(int _il, QString _name)
{
    InheritanceLevel = _il;
    name = _name;
    parents = new QVector<DrawingClass*>();
}

int DrawingClass::GetLevel()
{
    return InheritanceLevel;
}

void DrawingClass::Setlevel(int value)
{
    InheritanceLevel = value;
}

QString DrawingClass::GetName()
{
    return name;
}

void DrawingClass::levelUpdated(QMap<QString, DrawingClass *> *classes)
{
    foreach (DrawingClass* _class, *classes)
    {
        if (_class->parents->contains(this)){
            _class->Setlevel(this->InheritanceLevel-1);
            _class->levelUpdated(classes);
        }
    }
}

QRectF DrawingClass::boundingRect() const
{
    return QRect(-50,-25,100,50);
}

void DrawingClass::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setBrush(Qt::white);
    painter->drawRect(boundingRect());
    painter->drawText(boundingRect(), Qt::AlignCenter, name);
}

bool DrawingClass::operator==(DrawingClass& value)
{
return name == value.name;
}
