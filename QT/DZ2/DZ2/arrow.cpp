#include "arrow.h"
#include <QPen>
#include <QPainter>

Arrow::Arrow(DrawingClass* start, DrawingClass* end)
    : startItem(start), endItem(end)
{
    setPen(QPen(Qt::black, 2, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
}


QRectF Arrow::boundingRect() const
{
    qreal extra = (pen().width() + 20) / 2.0;

    return QRectF(line().p1(), QSizeF(line().p2().x() - line().p1().x(),
                                      line().p2().y() - line().p1().y()))
        .normalized()
        .adjusted(-extra, -extra, extra, extra);
}

void Arrow::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QPoint _start(startItem->boundingRect().center().x()+startItem->pos().x(),
                  startItem->boundingRect().bottom()+startItem->pos().y());
    QPoint _finish(endItem->boundingRect().center().x()+endItem->pos().x(),
                   endItem->boundingRect().top()+endItem->pos().y());
    setLine(QLine(_start,_finish));
    double angle = std::atan2(-line().dy(), line().dx());
    int arrowSize = 8;
    QPointF arrowP1 = line().p2() + QPointF(sin(angle + M_PI / 3) * -arrowSize,
                                       cos(angle + M_PI / 3) * -arrowSize);
    QPointF arrowP2 = line().p2() + QPointF(sin(angle + M_PI - M_PI / 3) * -arrowSize,
                                       cos(angle + M_PI - M_PI / 3) * -arrowSize);
    QPolygonF arrowhead;
    arrowhead.clear();
    arrowhead << line().p2() << arrowP1 << arrowP2;
    painter->setRenderHint(QPainter::Antialiasing);
    painter->drawLine(line());
    painter->setBrush(Qt::black);
    painter->drawPolygon(arrowhead);
}
