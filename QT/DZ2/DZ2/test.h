#ifndef DRAWINGCLASS_H
#define DRAWINGCLASS_H

#include <QGraphicsItem>
#include <QVector>

static class DrawingClass : public QGraphicsItem
{
    int InheritanceLevel;
    QString name;
    QRect boundary;
public:
    DrawingClass();
    DrawingClass(QString);
    DrawingClass(int,QString);
    QVector<DrawingClass*>* parents;
    int GetLevel();
    void Setlevel(int value);
    QString GetName() const;
    void connect(DrawingClass*,DrawingClass*,QGraphicsScene*);
    // QGraphicsItem interface
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
};

class B : public A
{

}
static class C : B
{

}
static class D
{

}

class C : public D , public B
{
}

class E : public C , public DrawingClass
{

}
#endif // DRAWINGCLASS_H
