#ifndef DRAWINGCLASS_H
#define DRAWINGCLASS_H

#include <QGraphicsItem>
#include <QVector>

class DrawingClass : public QGraphicsItem
{
    int InheritanceLevel;
    QString name;
    QRect boundary;
public:
    DrawingClass();
    DrawingClass(QString);
    DrawingClass(int,QString);
    bool operator==(DrawingClass&);
    QVector<DrawingClass*>* parents;
    int GetLevel();
    void Setlevel(int value);
    QString GetName();
    void levelUpdated(QMap<QString,DrawingClass*>*);
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
};

#endif // DRAWINGCLASS_H
