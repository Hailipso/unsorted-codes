#ifndef ARROW_H
#define ARROW_H

#include <QGraphicsLineItem>
#include "drawingclass.h"

class Arrow : public QGraphicsLineItem
{
private:
    DrawingClass* startItem,* endItem;
public:
    Arrow(DrawingClass*,DrawingClass*);
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
};

#endif // ARROW_H
