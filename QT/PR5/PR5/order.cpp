#include "order.h"

Order::Order(int _id, double _totalPrice,QString _customerName, bool _hasGifts){
    id = _id;
    totalPrice = _totalPrice;
    customerName = _customerName;
    hasGifts = _hasGifts;
}

void Order::Print(){
    qDebug() << "id = " << id << ", totalPrice = " << totalPrice
             << ", customerName = " << customerName << ", hasGifts = " << hasGifts;
}
