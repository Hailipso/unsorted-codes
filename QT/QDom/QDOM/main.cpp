#include <QCoreApplication>
#include <QDomDocument>
#include <QFile>
#include <QDir>
#include <QTextStream>
#include <QDebug>

struct student{
    int _id;
    QString fio;
};

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QList<student> students;
    student student = {1,"Ivanov"}, student2 = {2,"Bogdanov"},
            student3 = {3,"Nikitin"}, student4 = {4,"Mihailov"};
    students.append(student);
    students.append(student2);
    students.append(student3);
    students.append(student4);
    QFile file(QDir::homePath() + "\\QDomTest.xml");
    qDebug() << QDir::homePath();
    file.open(QIODevice::WriteOnly);
    QTextStream stream(&file);
    QDomDocument *doc = new QDomDocument();
    doc->setContent(stream.readAll());
    QDomElement root = doc->createElement("MIREA");
    QDomElement group1 = doc->createElement("BSBO-2020");
    QDomElement group2 = doc->createElement("BSBO-2021");
    root.appendChild(group1);
    root.appendChild(group2);
    for (int i=0;i<4;i++){
        QDomElement _student = doc->createElement("student");
        QDomElement shifr = doc->createElement("shifr");
        shifr.firstChild().setNodeValue(QString::number(students[i]._id));
        QDomElement fio = doc->createElement("fio");
        fio.firstChild().setNodeValue(students[i].fio);
        _student.appendChild(shifr);
        _student.appendChild(fio);
        if (i < 2) group1.appendChild(_student);
        else group2.appendChild(_student);
    }
    doc->;
    file.close();
    return a.exec();
}
