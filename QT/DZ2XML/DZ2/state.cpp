#include "state.h"
#include <QPainter>

State::State()
{
    inputs = new QVector<input>();
    QPainterPath* path = new QPainterPath();
    path->addEllipse(QPointF(0,0),20,20);
    PolyShape = new QPolygonF();
    *PolyShape = path->toFillPolygon();
}

State::State(QString name)
{
    inputs = new QVector<input>();
    QPainterPath* path = new QPainterPath();
    PolyShape = new QPolygonF();
    path->addEllipse(QPointF(0,0),20,20);
    *PolyShape = path->toFillPolygon();
    _name = name;
}

QString State::GetName()
{
    return _name;
}


QRectF State::boundingRect() const
{
    return QRectF(-20,-20,40,40);
}

void State::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setRenderHint(QPainter::Antialiasing);
    QBrush brush(QColor(Qt::white),Qt::SolidPattern);
    painter->setBrush(brush);
    painter->drawEllipse(QPointF(0,0),20,20);
    painter->drawText(QRectF(-20,-20,40,40),Qt::AlignCenter,_name);
    QGraphicsEllipseItem ellipse;
}

QPolygonF* State::GetPolygon()
{
    return PolyShape;
}
