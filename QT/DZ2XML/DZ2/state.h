#ifndef STATE_H
#define STATE_H
#include <QString>
#include <QList>
#include <QGraphicsItem>

struct input{
    QString InputName;
    QString NextState;
};

class State : public QGraphicsItem
{
    QString _name;
public:
    State();
    State(QString);

    QString GetName();
    QVector<input>* inputs;
    QPolygonF* PolyShape;
    float angle;
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QPolygonF* GetPolygon();
};

#endif // STATE_H
