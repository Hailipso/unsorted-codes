#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFile>
#include <QFileDialog>
#include <QTextStream>
#include <QDebug>
#include <QDomDocument>
#include <QtMath>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->graphicsView->setScene(&scene);
    connect(ui->LoadXMLBt,SIGNAL(clicked()),this,SLOT(LoadFile()));
    states = new QMap<QString,State*>();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::LoadFile()
{
//open file
    QString FileName = QFileDialog::getOpenFileName(this, "Open XML", "", "XML (*.xml)");
    if (FileName.isNull()) {qDebug() << "Open Error : File not found"; return;}
    QFile file(FileName);
    if (!file.open(QFile::ReadOnly | QFile::Text))
    {
        qDebug() << "Open error : File could not be opened.";
        return;
    }
    QTextStream stream(&file);
//parse XML to QDom
    QDomDocument* xdoc = new QDomDocument();
    if (!xdoc->setContent(stream.readAll().toLocal8Bit()))
    {
        qDebug() << "Parse error : XML empty or file cannot be parsed.";
        file.close();
        return;
    }
    file.close();
    states->clear();
//parse objects
    QDomElement root = xdoc->documentElement();
    QDomElement state = root.firstChildElement();
    while (!state.isNull())
    {
        State *object = new State(state.attribute("name"));
        QDomElement _input = state.firstChildElement();
        while (!_input.isNull())
        {
            object->inputs->append(*new input{_input.attribute("name"),
                                              _input.attribute("state")});
            _input = _input.nextSiblingElement();
        }
        states->insert(object->GetName(),object);
        state = state.nextSiblingElement();
    }
//draw states
    scene.clear();
    int radius = 20+20*states->size();
    int count = 0;
    for (auto i = states->begin(); i!=states->end();i++)
    {
        i.value()->setPos(QPointF(radius*qCos(2*M_PI*count/states->size()),
                                  radius*qSin(2*M_PI*count/states->size())));
        i.value()->setZValue(1);
        i.value()->angle = 2*M_PI*count/states->size();
        scene.addItem(i.value());
        count++;
    }
//draw input connections - TODO
    for (auto i = states->begin(); i!=states->end();i++)
    {
        QVector<input>* _inputs = i.value()->inputs;
        foreach (input _input, *_inputs)
        {
//            QPainterPath* path = new QPainterPath(QPoint(0,0));
//            path->clear(); path->quadTo(QPointF(50,50),QPointF(0,100));
//            scene.addPath(*path);
            //TODO: draw line/arc from one item to another
            //line
            Arrow* _arrow = new Arrow(i.value(), states->value(_input.NextState),_input.InputName);
            _arrow->setZValue(-1);
            _arrow->stackBefore(i.value());
            scene.addItem(_arrow);
            i.value()->update();
        }
    }
}
