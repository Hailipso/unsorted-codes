#ifndef ARROW_H
#define ARROW_H

#include <QGraphicsLineItem>
#include "state.h"

class Arrow : public QGraphicsLineItem
{
public:
    Arrow(State *startItem, State *endItem, QString inputName, QGraphicsItem *parent = nullptr);
    QRectF boundingRect() const override;
    State *startItem() const { return myStartItem; }
    State *endItem() const { return myEndItem; }
private:
    State *myStartItem;
    State *myEndItem;
    QString inputName;
    QPolygonF arrowHead;
    QColor myColor = Qt::blue;
protected:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget = nullptr) override;
};

#endif // ARROW_H
