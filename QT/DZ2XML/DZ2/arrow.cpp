#include "arrow.h"
#include <QPen>
#include <QPainter>
#include <QtMath>

Arrow::Arrow(State *startItem, State *endItem, QString inputname, QGraphicsItem *parent)
    : QGraphicsLineItem(parent), myStartItem(startItem), myEndItem(endItem), inputName(inputname)
{
    setPen(QPen(myColor, 2, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
}

QRectF Arrow::boundingRect() const
{
    qreal extra = (pen().width() + 20) / 2.0;

    return QRectF(line().p1(), QSizeF(line().p2().x() - line().p1().x(),
                                      line().p2().y() - line().p1().y()))
        .normalized()
        .adjusted(-extra, -extra, extra, extra);
}

void Arrow::paint(QPainter *painter, const QStyleOptionGraphicsItem *,
                  QWidget *)
{
    //initial setup
    QPen myPen = pen();
    myPen.setColor(myColor);
    qreal arrowSize = 8;
    painter->setPen(myPen);
    painter->setRenderHint(QPainter::Antialiasing);
  //if input enters same state
    if (myStartItem == myEndItem){
        QPainterPath* path = new QPainterPath();
        float _angle = myStartItem->angle;
        path->addEllipse(myStartItem->pos() + QPointF(cos(_angle)*20,sin(_angle)*20),15,15);
        painter->drawPath(*path);
        QPointF arrowP1 = path->pointAtPercent(fmod(_angle/(2*M_PI) + 0.28, 1)) +
                                        QPointF(sin(_angle + M_PI / 3) * arrowSize,
                                                cos(_angle + M_PI / 3) * -arrowSize);
        QPointF arrowP2 = path->pointAtPercent(fmod(_angle/(2*M_PI) + 0.28, 1)) +
                                        QPointF(sin(_angle + M_PI - M_PI / 3) * arrowSize,
                                                cos(_angle + M_PI - M_PI / 3) * -arrowSize);
        arrowHead.clear();
        arrowHead << path->pointAtPercent(fmod(_angle/(2*M_PI) + 0.28, 1)) << arrowP1 << arrowP2;
        QPointF textPoint;
        if (path->angleAtPercent(fmod(_angle/(2*M_PI) + 0.28, 1)) > 150 &&
                path->angleAtPercent(fmod(_angle/(2*M_PI) + 0.28, 1)) < 300)
            textPoint = myStartItem->pos() + QPointF(cos(_angle)*40,sin(_angle)*40);
        else if (path->angleAtPercent(fmod(_angle/(2*M_PI) + 0.28, 1)) > 330)
            textPoint = myStartItem->pos() + QPointF(cos(_angle)*40-5*inputName.length(),sin(_angle)*40);
        else textPoint = myStartItem->pos() + QPointF(cos(_angle)*40,sin(_angle)*40 + 10);
        painter->setBrush(myColor);
        painter->drawPolygon(arrowHead);
        painter->setPen(Qt::black);
        painter->drawText(textPoint,inputName);
    }
    if (myStartItem->collidesWithItem(myEndItem))
        return;
    //get intersect
    QLineF centerLine(myStartItem->pos(), myEndItem->pos());
    QPolygonF* endPolygon = myEndItem->GetPolygon();
    QPointF p1 = endPolygon->first() + myEndItem->pos();
    QPointF intersectPoint;
    for (int i = 1; i < endPolygon->count(); ++i) {
        QPointF p2 = endPolygon->at(i) + myEndItem->pos();
        QLineF polyLine = QLineF(p1, p2);
        QLineF::IntersectionType intersectionType =
            polyLine.intersects(centerLine, &intersectPoint);
        if (intersectionType == QLineF::BoundedIntersection)
            break;
        p1 = p2;
    }
    setLine(QLineF(intersectPoint, myStartItem->pos()));
    //get angle, text and control point
    qreal dy = -line().dy(), dx = line().dx();
    QPointF ctrlPoint,edgePoint,textPoint;
    if (dy > 0 && dx > 0)
    {
        edgePoint = boundingRect().bottomRight() + QPointF(0,-boundingRect().height()/4);
        ctrlPoint = edgePoint + QPointF(-boundingRect().width()/3.5,0);
    }
    else if (dy < 0 && dx > 0)
    {
        edgePoint = boundingRect().bottomLeft() + QPointF(boundingRect().width()/5,0);
        ctrlPoint = edgePoint + QPointF(0,-boundingRect().height()/3.5);
    }
    else if (dy < 0 && dx < 0)
    {
        edgePoint = boundingRect().topLeft()+ QPointF(0,boundingRect().height()/4);
        ctrlPoint = edgePoint + QPointF(boundingRect().width()/3.5,0);
    }
    else
    {
        edgePoint = boundingRect().topRight() + QPointF(-boundingRect().width()/5,0);;
        ctrlPoint = edgePoint + QPointF(0,+boundingRect().height()/3.5);
    }
    QPainterPath path(myStartItem->pos());
    path.quadTo(ctrlPoint,intersectPoint);
    QLineF _line(myEndItem->pos(),edgePoint);
    double angle = std::atan2(-_line.dy(), _line.dx());
    QPointF arrowP1 = line().p1() + QPointF(sin(angle + M_PI / 3) * arrowSize,
                                            cos(angle + M_PI / 3) * arrowSize);
    QPointF arrowP2 = line().p1() + QPointF(sin(angle + M_PI - M_PI / 3) * arrowSize,
                                            cos(angle + M_PI - M_PI / 3) * arrowSize);
    if (dy > 0 && dx > 0)
    {
        textPoint = line().p1() + QPointF(sin(angle + M_PI / 3) * arrowSize * 1.5,
                                           cos(angle + M_PI / 3) * arrowSize * 1.5 + 10);
    }
    else if (dy < 0 && dx > 0)
    {
        textPoint = line().p1() + QPointF(sin(angle + M_PI / 3) * arrowSize * 1.5-5*inputName.length(),
                                           cos(angle + M_PI / 3) * arrowSize * 1.5+10);
    }
    else if (dy < 0 && dx < 0)
    {
        textPoint = line().p1() + QPointF(sin(angle + M_PI / 3) * arrowSize * 1.5-5*inputName.length(),
                                           cos(angle + M_PI / 3) * arrowSize * 1.5);
    }
    else
    {
        textPoint = line().p1() + QPointF(sin(angle + M_PI / 3) * arrowSize * 1.5,
                                           cos(angle + M_PI / 3) * arrowSize * 1.5);
    }
    arrowHead.clear();
    arrowHead << line().p1() << arrowP1 << arrowP2;
    painter->drawPath(path);
    painter->setBrush(myColor);
    painter->drawPolygon(arrowHead);
    painter->setPen(Qt::black);
    painter->drawText(textPoint,inputName);
}
