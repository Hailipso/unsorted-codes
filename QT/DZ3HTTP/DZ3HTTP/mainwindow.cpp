#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDateTime>
#include <QSqlQuery>
#include <QSqlError>
#include <QXmlStreamReader>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QRegularExpression>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    timer = new QTimer(this);
    timer->stop();
    connect(timer, SIGNAL(timeout()),
            this, SLOT(SeekChanges()));
    connect(this->ui->sendBt, SIGNAL(clicked()),
            this, SLOT(StartLooking()));
    qDebug() << QSslSocket::sslLibraryBuildVersionString();
    qDebug() << QSslSocket::supportsSsl();
    qDebug() << QSslSocket::sslLibraryVersionString();
    //connect to database - TODO
//    qDebug() << QSqlDatabase::drivers();
//    db.setDatabaseName("log.db");
//    if (db.open()) qDebug() << "db opened!";
//    else qDebug() << db.lastError();
//    //create table if none exists
//    QSqlQuery query;
//    query.exec("CREATE TABLE IF NOT EXISTS Logs ( "
//    "ID INTEGER PRIMARY KEY AUTOINCREMENT , "
//    "type VARCHAR(8) NOT NULL , "
//    "datetime DATETIME NOT NULL , "
//    "message TEXT NOT NULL )");
}

MainWindow::~MainWindow()
{
    delete ui;
}
/* external
tree_node_<HTML::Node> MainWindow::getElement(tree<HTML::Node> dom,
                                                 QString XPath,
                                                 int maxDepth, int currentDepth)
{
    int currentoffset = 1;
    for (auto child = dom.begin(); child!= dom.end(); ++child)
    {
        if (child.node->data.tagName() ==
                XPath.section('/',currentDepth,currentDepth).toStdString())
        {
            if (currentoffset == offsets.at(currentDepth))
                return *child.node;
            tree_node_<HTML::Node> result =
                    getElement(child,XPath,maxDepth,++currentDepth);
            return result;
        }
    }
    return tree_node_<HTML::Node>();
}
*/
void MainWindow::StartLooking()
{
    timer->stop();
    //Get List of Strings contained in URL
    _url = ui->lineEditURL->text();
    _xpath = ui->lineEditXPath->text();
    if (_xpath.at(0) == '/') _xpath = _xpath.section('/',1);
    //get HTML
    QNetworkAccessManager manager;
    QNetworkReply *response = manager.get(QNetworkRequest(QUrl(_url)));
    QEventLoop event;
    connect(response,SIGNAL(finished()),&event,SLOT(quit()));
    event.exec();
    QString html = response->readAll();\
    //parse HTML
    /* external - htmlcxx
    HTML::ParserDom parser;
    tree<HTML::Node> dom = parser.parseTree(html.toStdString());
    offsets.clear();
    for (int i = 0; i <= _xpath.count('/'); i++)
    {
        QString section = _xpath.section('/',i,i);
        if (_xpath.section('/',i,i).contains('[')) // [number]
        {

            QString number = _xpath.section('/',i,i).
                    section('[',1,1).
                    section(']',0,0);
            offsets.append(_xpath.section('/',i,i).
                           section('[',1,1).
                           section(']',0,0).toInt());
        }
        else offsets.append(1);
    }
    tree_node_<HTML::Node> element = getElement(dom, _xpath, _xpath.count('/'),0);
    */


    /* xml <- might work with clear xml, possible to load and udpate offline xhtml(?) files
    QXmlStreamReader reader(html);
    int max_depth = _xpath.count('/'), depth = 0;
    offsets.clear();
    for (int i = 0; i <= max_depth; i++)
    {
        QString section = _xpath.section('/',i,i);
        if (_xpath.section('/',i,i).contains('[')) // [number]
        {

            QString number = _xpath.section('/',i,i).
                    section('[',1,1).
                    section(']',0,0);
            offsets.append(_xpath.section('/',i,i).
                           section('[',1,1).
                           section(']',0,0).toInt());
        }
        else offsets.append(1);
    }
    int currentoffset = 1;
    while (!reader.atEnd())
    {
        QXmlStreamReader::TokenType type = reader.readNext();
        QStringRef _text = reader.name();
        if (type == QXmlStreamReader::StartElement)
        {
            if (_text. contains(_xpath.section('/',depth,depth)))
            {
                if (currentoffset < offsets.at(depth))
                {
                    currentoffset++;
                    reader.readElementText();
                }
                else
                {
                    if (depth == max_depth) break;
                    depth++;
                    currentoffset = 1;
                }
            }
            else {reader.readElementText(QXmlStreamReader::SkipChildElements);
            QXmlStreamReader::TokenType type2 = reader.readNext();
            QStringRef _text2 = reader.name();
            int wait = 0;
            }
        }
    }
    */
    //getElement End

    //Show results
    QStringList list;
    list.append(QString::fromStdString(element.data.text()));
    if (list.empty() || list.at(0).isEmpty()) // if empty - wrong path(s)
    {
        qDebug() << "error: Page by this URL or Element by this XPath does not exists.";
        return;
    }
    if (list.count() > 1) // if > 1 - not precise enough
    {
        qDebug() << "error: More than one element exists, please be more precise.";
        return;
    }
    lookout = list.first(); //element content
    QString time = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
    ui->textEdit->append(time + " Started looking to changes in element " +
                         ui->lineEditXPath->text() +
                         " at " + ui->lineEditURL->text() +
                         "\nCurrent Element content is:\n"+ lookout);
    AddLog(time,ui->lineEditURL->text(),ui->lineEditXPath->text(),lookout);
    timer->start(5000); //look for changes every 30 seconds
}

void MainWindow::SeekChanges()
{\
    //get new HTML
    QNetworkAccessManager manager;
    QNetworkReply *response = manager.get(QNetworkRequest(QUrl(_url)));
    QEventLoop event;
    connect(response,SIGNAL(finished()),&event,SLOT(quit()));
    event.exec();
    QString html = response->readAll();
    //Parse HTML, xml here
    QXmlStreamReader reader(html);
    int max_depth = _xpath.count('/'), depth = 0;
    offsets.clear();
    for (int i = 0; i <= max_depth; i++)
    {
        if (_xpath.section('/',depth,depth).contains('[')) // [number]
        {
            offsets.append(_xpath.section('/',depth,depth).
                           section('[',1,1).
                           section(']',0,0).toInt());
        }
        else offsets.append(1);
    }
    int currentoffset = 1;
    while (!reader.atEnd())
    {
        QXmlStreamReader::TokenType type = reader.readNext();
        QStringRef _text = reader.name();
        if (type == QXmlStreamReader::StartElement && _text.
                contains(_xpath.section('/',depth,depth)))
        {
            if (currentoffset < offsets.at(depth))
            {
                currentoffset++;
                reader.readElementText();
            }
            else
            {
                if (depth == max_depth) break;
                depth++;
                currentoffset = 1;
            }
        }
        else reader.readElementText();
    }
    //getElement end

    //show if updated
    QStringList list;
    list.append(reader.readElementText(QXmlStreamReader::IncludeChildElements));
    if (list.empty() || list.count() > 1)
    {
        ui->textEdit->append("Page changed or no longer available, stopping lookout.");
        timer->stop();
        return;
    }
    QString compare = list.first();
    if (compare == lookout) return;
    ui->textEdit->append("Element content changed. New content:\n" +
                         compare);
    lookout = compare;
    QString time = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
    AddLog(time,ui->lineEditURL->text(),ui->lineEditXPath->text(),lookout);
}

void MainWindow::AddLog(QString time, QString Url, QString XPath, QString Element)
{
    // TODO - change DB format
//    QSqlQuery query;
//    query.prepare("INSERT INTO Logs (type, datetime, message) VALUES"
//"(:type,:datetime,:message)");
//    if (type == sent) query.bindValue(":type", "sent");
//        else query.bindValue(":type", "recieved");
//    query.bindValue(":datetime", time);
//    query.bindValue(":message", message);
    //    query.exec();
}
