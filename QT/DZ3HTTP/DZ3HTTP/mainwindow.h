#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSqlDatabase>
#include <QHostAddress>
#include <QTimer>


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE
using namespace htmlcxx;
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void AddLog(QString,QString,QString,QString);
    void htmlGet(const QUrl &url, const std::function<void(const QString&)> &fun);
    //tree_node_<HTML::Node> getElement(tree<HTML::Node> dom, QString XPath, int maxDepth, int currentDepth);
public slots:
    void StartLooking();
    void SeekChanges();

private:
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    QString lookout, _url, _xpath;
    QTimer *timer;
    QVector<int> offsets;
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
