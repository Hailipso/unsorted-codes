#include "history.h"
#include "ui_history.h"
#include <QSqlQuery>

History::History(QWidget *parent):
    QDialog(parent),
    ui(new Ui::History)
{
    ui->setupUi(this);
    QSqlQuery query;
    query.exec("SELECT datetime , type , message FROM Logs ORDER BY datetime");
    while (query.next())
    {
        QString text = QString("%1 Message %2   >   %3").
                arg(query.value(0).toString()).
                arg(query.value(1).toString()).
                arg(query.value(2).toString());
        ui->textEdit->append(text);
    }
}

History::~History()
{
    delete ui;
}
