#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QUdpSocket>
#include <QSqlDatabase>
#include <QHostAddress>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    enum MessageType : bool
    {
        sent = true,
        recieved = false
    };

    void AddLog(QString,QString,MessageType);

public slots:
    void SetListeningPort();
    void SetRecieveingPort();
    void SetIP();
    void SendMessage();
    void ReadMessage();
private:
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    QUdpSocket* Listening,* Recieving;
    QHostAddress* IP;
    int ListeningPort, RecievingPort;
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
