#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "history.h"
#include <QInputDialog>
#include <QHostAddress>
#include <QRegularExpression>
#include <QNetworkDatagram>
#include <QTextStream>
#include <QDateTime>
#include <QSqlQuery>
#include <QSqlError>
#include <QtGlobal>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    //setup sockets
    Recieving = new QUdpSocket();
    Listening = new QUdpSocket();
    IP = new QHostAddress(QHostAddress::LocalHost);
    ui->setupUi(this);
    //set connections
    connect(ui->actionHistory,&QAction::triggered,
            this,[=]() {History* history = new History(this);
                        history->exec();
                        delete history;});
    connect(ui->actionListeningPort,&QAction::triggered,
            this,&MainWindow::SetListeningPort);
    connect(ui->actionRecievingPort,&QAction::triggered,
            this,&MainWindow::SetRecieveingPort);
    connect(ui->actionRecievingIP,&QAction::triggered,
            this,&MainWindow::SetIP);
    connect(ui->SendBt,&QPushButton::clicked,
            this,&MainWindow::SendMessage);
    //setup ui
    ui->SendBt->setDisabled(true);
    ui->lineEdit->setDisabled(true);
    ui->textEdit->setText("Notice: Please setup ports to begin");
    //connect to database
    db.setDatabaseName("log.db");
    if (db.open()) qDebug() << "db opened!";
    else qDebug() << db.lastError();
    //create table if none exists
    QSqlQuery query;
    query.exec("CREATE TABLE IF NOT EXISTS Logs("
    "ID INTEGER PRIMARY KEY AUTOINCREMENT,"
    "type VARCHAR(8) NOT NULL,"
    "datetime DATETIME NOT NULL,"
    "message TEXT NOT NULL)");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::SetListeningPort()
{
    bool ok = false;
    ListeningPort = QInputDialog::getInt(this,"Enter Listening Port", "Port",ListeningPort,1,65355,1, &ok);
    if (ok)
    {
        Listening->disconnectFromHost();
        Listening->waitForDisconnected();
        Listening->bind(QHostAddress::Any, ListeningPort);
        connect(Listening,&QUdpSocket::readyRead,
                this,&MainWindow::ReadMessage);
        ui->textEdit->append("Listening on port " + QString::number(ListeningPort));
    }
}

void MainWindow::SetRecieveingPort()
{
    bool ok = false;
    RecievingPort = QInputDialog::getInt(this,"Enter Recieving Port", "Port",RecievingPort,1,65355,1,&ok);
    if (ok){
        ui->textEdit->append("Sending to " + IP->toString() + ":" + QString::number(RecievingPort));
        ui->lineEdit->setEnabled(true);
        ui->SendBt->setEnabled(true);
    }
}

void MainWindow::SetIP()
{
    bool ok = false;
    QString _ipText = QInputDialog::getText(this,"Enter Recieving IPv4", "IP",QLineEdit::Normal,IP->toString(),&ok);
    QRegularExpression regex("^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$");
    QRegularExpressionMatch match = regex.match(_ipText);
    if (ok && match.hasMatch())
    {
        IP = new QHostAddress(_ipText);
        ui->textEdit->append("Sending to " + IP->toString() + ":" + QString::number(RecievingPort));
    }
    else if (ok) ui->textEdit->append("Error: Invalid IP");
}

void MainWindow::SendMessage()
{
    QString text = ui->lineEdit->text();
    if (text.size() > 400)
    {
        ui->textEdit->append("Error: Message too long");
    }
    if (!text.isEmpty())
    {
        QString time = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
        QString message = time + "   >   " + text;
        QTextStream stream(&message, QIODevice::WriteOnly);
        Recieving->writeDatagram(stream.readAll().toUtf8(),*IP,RecievingPort);
        ui->textEdit->append("Message sent: " + message);
        ui->lineEdit->clear();
        AddLog(time,text,sent);
    }
}

void MainWindow::ReadMessage()
{
    while (Listening->hasPendingDatagrams())
    {
        QNetworkDatagram datagram = Listening->receiveDatagram();
        QTextStream stream(datagram.data(),QIODevice::ReadOnly);
#if QT_VERSION >= 0x060000
        stream.setEncoding(QStringConverter::Utf8);
#else
        stream.setCodec("UTF-8");
#endif
        QString message = stream.readAll();
        ui->textEdit->append("Message recieved: " + message);
        QString time = message.section("   >",0,0);
        QString text = message.section(">   ",1);
        AddLog(time,text,recieved);
    }
}


void MainWindow::AddLog(QString time, QString message, MessageType type)
{
    QSqlQuery query;
    query.prepare("INSERT INTO Logs (type, datetime, message) VALUES"
"(:type,:datetime,:message)");
    if (type == sent) query.bindValue(":type", "sent");
        else query.bindValue(":type", "recieved");
    query.bindValue(":datetime", time);
    query.bindValue(":message", message);
    query.exec();
}
