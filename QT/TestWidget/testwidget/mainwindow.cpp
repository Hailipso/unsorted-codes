#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "qdebug.h"
#include "qfiledialog.h"
#include "qstring.h"
#include "qtextstream.h"
#include <QDataStream>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->OpenFileBt, SIGNAL(clicked()), this, SLOT(openFile()));
    connect(ui->SaveFileBt, SIGNAL(clicked()), this, SLOT(saveFile()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::openFile()
{
    qDebug() << "Open File";
    openBinaryFile();
}

void MainWindow::openTextFile(){

    QString FileName = QFileDialog::getOpenFileName(this, tr("Открытие файла"),"" ,"All files (*.*)");
    if (FileName.isEmpty()) {return;}
    QFile File(FileName);

    bool isSuccessOpen = File.open(QFile::ReadOnly | QFile::Text);
    if (!isSuccessOpen) {
        qDebug() << "fail";
        return;
    }

    QTextStream stream(&File);
    QString allData = stream.readAll();

    ui->textEdit->setText(allData);
    File.close();
}

void MainWindow::openBinaryFile(){

    QString FileName = QFileDialog::getOpenFileName(this, tr("Открытие файла"),"" ,"All files (*.*)");
    if (FileName.isEmpty()) {return;}
    QFile File(FileName);

    bool isSuccessOpen = File.open(QFile::ReadOnly | QFile::Text);
    if (!isSuccessOpen) {
        qDebug() << "fail";
        return;
    }

    QTextStream stream(&File);
    QString allData = stream.readAll();

    ui->textEdit->setText(allData);
    File.close();
}

void MainWindow::saveFile()
{
    qDebug() << "Save File";
    saveBinaryFile();
}

void MainWindow::saveTextFile(){
    QString FileName = QFileDialog::getSaveFileName(this, tr("Сохранение файла"), "","All files (*.*)");
    if (FileName.isEmpty()) {return;}
    QFile File(FileName);
    bool isSuccessOpen = File.open(QFile::WriteOnly | QFile::Text);
    if (!isSuccessOpen) {
        qDebug() << "fail";
        return;
    }
    QTextStream stream(&File);
    QString allData = ui->textEdit->toPlainText();
    stream << allData;
    //File.write(allData.toUtf8());
    File.close();
}

void MainWindow::saveBinaryFile(){
    QString FileName = QFileDialog::getSaveFileName(this, tr("Сохранение файла"), "","All files (*.*)");
    if (FileName.isEmpty()) {return;}
    QFile File(FileName);
    bool isSuccessOpen = File.open(QFile::WriteOnly | QFile::Text);
    if (!isSuccessOpen) {
        qDebug() << "fail";
        return;
    }
    QDataStream stream(&File);
    QString allData = ui->textEdit->toPlainText();
    stream << allData;
    //File.write(allData.toUtf8());
    File.close();
}
