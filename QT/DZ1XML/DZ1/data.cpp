#include "data.h"
#include <QObject>
#include <QString>
#include <QDomElement>

Data::Data()
{
    ObjectID = 0;
    Region = "";
    Type = "";
    SafetyRating = "";
    Owner = "";
    PersonalData = "";
}

Data::Data(const QDomElement &object)
{
    ObjectID = object.attribute("ObjectID").toInt();
    Region = object.attribute("Region");
    Type = object.attribute("Type");
    SafetyRating = object.attribute("SafetyRating");
    Owner = object.attribute("Owner");
    PersonalData = object.attribute("PersonalData");
}
QDomElement Data::ToXML(QDomElement result)
{
    result.setAttribute("ObjectID", QString::number(ObjectID));
    result.setAttribute("Region", Region);
    result.setAttribute("Type", Type);
    result.setAttribute("SafetyRating", SafetyRating);
    result.setAttribute("Owner", Owner);
    result.setAttribute("PersonalData",PersonalData);
    return result;
}

QString Data::GetProperty(int PropID) const
{
    switch(PropID){
        case(0): return QString::number(ObjectID); break;
        case(1): return Region; break;
        case(2): return Type; break;
        case(3): return SafetyRating; break;
        case(4): return Owner; break;
        case(5): return PersonalData; break;
    default: return "";
    }
}

void Data::SetProperty(int PropID, QString property)
{
    switch(PropID)
    {
        case(0): ObjectID = property.toInt(); break;
        case(1): Region = property; break;
        case(2): Type = property; break;
        case(3): SafetyRating = property; break;
        case(4): Owner = property; break;
        case(5): PersonalData = property; break;
        default: return;
    }
}
