#ifndef DATAMODEL_H
#define DATAMODEL_H

#include <QAbstractTableModel>
#include <QObject>
#include <data.h>

class DataModel : public QAbstractTableModel
{
    QMap<int,Data*>*modelList;
    Q_OBJECT
public:
    explicit DataModel(QObject *parent = nullptr);
    int rowCount(const QModelIndex &parent) const;
    int rowCount() const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);
    Qt::ItemFlags flags(const QModelIndex &index) const;
    void clear();
    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex());
    bool insertRows(int row, int count, Data *object, const QModelIndex &parent = QModelIndex());
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex());
    const QMap<int,Data*>* getMap();
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
};

#endif // DATAMODEL_H
