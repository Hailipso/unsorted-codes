#include "datamodel.h"


DataModel::DataModel(QObject *parent) : QAbstractTableModel(parent)
{
    modelList = new QMap<int,Data*>();

}

int DataModel::rowCount(const QModelIndex &parent) const
{
    return modelList->keys().size();
}

int DataModel::rowCount() const
{
    return modelList->keys().size();
}

int DataModel::columnCount(const QModelIndex &parent) const
{
    return 6;
}

QVariant DataModel::data(const QModelIndex &index, int role) const
{
    if (role == Qt::DisplayRole || role == Qt::EditRole)
    {
        int i = index.row();
        int j = index.column();
        //return this->modelList->value(i)->GetProperty(j);
        return this->modelList->value(modelList->keys().at(i))->GetProperty(j);
    }
    else return QVariant();
}

bool DataModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    modelList->value(modelList->keys().at(index.row()))->SetProperty(index.column(),value.toString());
    emit dataChanged(index,index);
    return true;
}

Qt::ItemFlags DataModel::flags(const QModelIndex &index) const
{
    return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable;
}

void DataModel::clear(){
    beginResetModel();
    delete modelList;
    modelList = new QMap<int,Data*>;
    endResetModel();
}

bool DataModel::insertRows(int row, int count, const QModelIndex &parent)
{
    beginInsertRows(parent,row,row+count-1);
    modelList->insert(row, new Data);
    endInsertRows();
    return true;
}

bool DataModel::insertRows(int row, int count, Data *object, const QModelIndex &parent)
{
    beginInsertRows(parent,row,row+count-1);
    modelList->insert(row, object);
    endInsertRows();
    return true;
}

bool DataModel::removeRows(int row, int count, const QModelIndex &parent)
{
    if (row < 0 || row > modelList->count()) return false;
    beginRemoveRows(parent,row,row+count-1);
    for (int i = 0; i < count; ++i){
        modelList->remove(modelList->keys().at(row));
        foreach (int key, modelList->keys())
        {
            if (key>row){
                Data* value = modelList->take(key);
                modelList->insert(key-1,value);
            }
        }
    }
    endRemoveRows();
    return true;
}

const QMap<int,Data *>* DataModel::getMap()
{
    return modelList;
}


QVariant DataModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Horizontal)
        switch (section){
            case 0: return "ObjectID";
            case 1: return "Region";
            case 2: return "Type";
            case 3: return "Safety Rating";
            case 4: return "Owner";
            case 5: return "Personal Data";
        }
    return QVariant();
}
