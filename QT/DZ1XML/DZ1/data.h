#ifndef DATA_H
#define DATA_H

#include <QObject>
#include <QDomElement>

class Data
{
    int ObjectID;
    QString Region, Type, SafetyRating, Owner, PersonalData;
public:
    Data();
    Data(const QDomElement &object);
    QDomElement ToXML(QDomElement);
    QString GetProperty(int) const;
    void SetProperty(int,QString);
};

#endif // DATA_H
