#include "data.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QDataStream>
#include <QFile>
#include <QTextStream>
#include <QDomDocument>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
//create view, container, model
    ui->setupUi(this);
    model = new DataModel();
//prepare view
    ui->tableView->setModel(model);
    ui->tableView->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
//connect
    connect(ui->OpenFileBt,SIGNAL(clicked()),this,SLOT(OpenFile()));
    connect(ui->SaveFileBt,SIGNAL(clicked()),this,SLOT(SaveFile()));
    connect(ui->InsertRowBt,SIGNAL(clicked()),this,SLOT(InsertRow()));
    connect(ui->RemoveRowBt,SIGNAL(clicked()),this,SLOT(DeleteRow()));
}

MainWindow::~MainWindow()
{
    delete ui;
    delete model;
}

void MainWindow::OpenFile()
{
//Open File
    QString FileName = QFileDialog::getOpenFileName(this, "Open XML", "", "XML (*.xml)");
    if (FileName.isNull()) {qDebug() << "Open Error : File not found"; return;}
    QFile file(FileName);
    if (!file.open(QFile::ReadOnly | QFile::Text))
    {
        qDebug() << "Open error : File could not be opened.";
        return;
    }
    QTextStream stream(&file);
//Parse XML to QDom
    QDomDocument* xdoc = new QDomDocument();
    if (!xdoc->setContent(stream.readAll().toLocal8Bit()))
    {
        qDebug() << "Parse error : XML empty or file cannot be parsed.";
        file.close();
        return;
    }
    file.close();
//Clear old data
    model->clear();
//Parse QDom to Objects and Fill Data
    QDomElement root = xdoc->documentElement();
    QDomElement xobject = root.firstChildElement();
    int count = 0;
    while (!xobject.isNull())
    {
        Data *object = new Data(xobject);
        model->insertRows(count,1,object);
        xobject = xobject.nextSiblingElement();
        count++;
    }
    return;
}

void MainWindow::SaveFile()
{
    QDomDocument* xdoc = new QDomDocument();
    QDomElement root = xdoc->createElement("table");
    xdoc->appendChild(root);
    auto keys = model->getMap()->keys();
    foreach (int i , keys)
    {
        QDomElement xobject = xdoc->createElement("object");
        xobject = model->getMap()->value(i)->ToXML(xobject);
        root.appendChild(xobject);
    }
    QString xmltext = xdoc->toString();
//save to file
    QString FileName = QFileDialog::getSaveFileName(this, "Save XML", "", "XML (*.XML)");
    QFile file(FileName);
    if (file.open(QFile::WriteOnly | QFile::Text))
    {
        QTextStream stream(&file);
        stream << xmltext;
        file.close();
    }
    else
    {
        qDebug() << "Save error : File cannot be opened/created.";
    }
}

void MainWindow::InsertRow()
{
    model->insertRows(model->rowCount(),1);
}

void MainWindow::DeleteRow(){
    QModelIndexList indexList = ui->tableView->selectionModel()->selectedIndexes();
    if (indexList.count() != 0)
        model->removeRows(indexList.first().row(),indexList.count());
}
