#ifndef STUDENTLIST_H
#define STUDENTLIST_H
#include <QString>
#include <QMap>

struct Student{
int srifr;
QString fio;
QString group;
};

class StudentList{
    QMap<int,Student> students;
public:
    StudentList();
    ~ StudentList();
    Student at(int shifr) const;
    Student at(const QString &fio) const;
    void add(const Student &s);
    void remove(int shifr) const;
    void remove(const QString &fio) const;
};



#endif // STUDENTLIST_H
