package main

import (
	"fmt"
	"unicode/utf8"

	_ "github.com/lib/pq"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/widget"
)

func CreateWorkWindow(a fyne.App) fyne.Window {
	w := a.NewWindow("SecureDB")
	w.SetMaster()
	w.Resize(fyne.NewSize(1000, 500))

	var data [][]string = [][]string{{}}
	var _layout *fyne.Container
	var list *widget.Table

	config_bt := fyne.NewMenuItem("Сервисы", func() {
		selectConfiguration(&data)
		updateTableWidth(&list, data)
	})
	config_bt.Disabled = !(li.isAuthorized)

	logs_bt := fyne.NewMenuItem("Логи", func() {
		selectLogs(&data)
		updateTableWidth(&list, data)
	})
	logs_bt.Disabled = !(li.isAdmin || li.isSecurity)

	audit_bt := fyne.NewMenuItem("Аудит", func() {
		selectAudit(&data)
		updateTableWidth(&list, data)
	})
	audit_bt.Disabled = !(li.isSecurity)

	report_bt := fyne.NewMenuItem("Создать отчет", func() {
		err := createReport(data, w)
		if err != nil {
			dialog.ShowError(err, w)
		}
	})
	report_bt.Disabled = !(li.isAuthorized)

	action_bt := fyne.NewMenuItem("Сделать запрос", func() {
		actionIntercept(w)
	})
	action_bt.Disabled = true

	perm_bt := fyne.NewMenuItem("Обновить разрешения", func() { permIntercept(w) })
	perm_bt.Disabled = true

	userUpdate_bt := fyne.NewMenuItem("Обновить пользователей", func() {
		if err := userUpdate(); err != nil {
			dialog.ShowError(err, w)
		} else {
			dialog.ShowInformation("Успешно", "Обновление пользователей успешно завершено", w)
		}
	})
	userUpdate_bt.Disabled = true

	userInsert_bt := fyne.NewMenuItem("Добавить пользователя", func() {
		w_reg := CreateRegistrationWindow(a)
		w_reg.Show()
	})
	userInsert_bt.Disabled = true

	login_win_bt := fyne.NewMenuItem("Авторизация", func() {
		w_login := CreateLoginWindow(a)
		w_login.SetOnClosed(func() {
			selectConfiguration(&data)
			updateTableWidth(&list, data)
			config_bt.Disabled = !(li.isAuthorized)
			report_bt.Disabled = !(li.isAuthorized)
			logs_bt.Disabled = !(li.isAdmin || li.isSecurity)
			action_bt.Disabled = !(li.isAuthorized)
			audit_bt.Disabled = !(li.isSecurity)
			perm_bt.Disabled = !(li.isSecurity)
			userUpdate_bt.Disabled = !(li.isSecurity)
			userInsert_bt.Disabled = !(li.isSecurity)
		})
		w_login.Show()
	})

	menu_login := fyne.NewMenu("Авторизация", login_win_bt)
	menu_tables := fyne.NewMenu("Таблицы",
		config_bt, logs_bt, audit_bt)
	menu_actions := fyne.NewMenu("Действия", action_bt, report_bt)
	menu_adminTools := fyne.NewMenu("Администрирование", perm_bt, userInsert_bt, userUpdate_bt)

	main_menu := fyne.NewMainMenu(menu_login, menu_tables, menu_actions, menu_adminTools)

	list = widget.NewTable(
		func() (int, int) {
			return len(data), len(data[0])
		},
		func() fyne.CanvasObject {

			return widget.NewLabel("very very wide content")
		},
		func(i widget.TableCellID, o fyne.CanvasObject) {
			o.(*widget.Label).SetText(data[i.Row][i.Col])
		})
	updateTableWidth(&list, data)
	list.OnSelected = func(id widget.TableCellID) {
		if id.Row == 0 {
			return
		}
		w.Clipboard().SetContent(data[id.Row][id.Col])
	}

	_layout = container.NewMax(list)

	w.SetMainMenu(main_menu)
	w.SetContent(_layout)
	w.Show()

	return w
}

func updateTableWidth(tab **widget.Table, data [][]string) {
	for i := 0; i < len(data[0]); i++ {
		max_string := ""
		for j := 0; j < len(data); j++ {
			if utf8.RuneCountInString(max_string) < utf8.RuneCountInString(data[j][i]) {
				max_string = data[j][i]
			}
		}
		(*tab).SetColumnWidth(i,
			fyne.MeasureText(max_string, 16, fyne.TextStyle{}).Width+4)
	}
}

func permIntercept(w fyne.Window) {
	acc_entry, serv_entry, dev_entry, model_entry, access_rb := widget.NewEntry(),
		widget.NewEntry(),
		widget.NewEntry(),
		widget.NewEntry(),
		widget.NewRadioGroup([]string{"Allow", "Deny"}, func(p string) {})
	dialog.ShowForm("Ввод разрешения", "Ввести", "Отмена",
		[]*widget.FormItem{
			widget.NewFormItem("Адрес", serv_entry),
			widget.NewFormItem("Аккаунт", acc_entry),
			widget.NewFormItem("Модель", model_entry),
			widget.NewFormItem("MAC-адрес", dev_entry),
			widget.NewFormItem("Доступ", access_rb),
		}, func(ok bool) {
			if !ok {
				return
			}
			err := updatePermissions(serv_entry.Text, acc_entry.Text, dev_entry.Text, access_rb.Selected, model_entry.Text)
			if err != nil {
				dialog.ShowError(err, w)
			}

		}, w)
}

func actionIntercept(w fyne.Window) {
	socket_entry, content_entry := widget.NewEntry(), widget.NewEntry()
	dialog.ShowForm("Запрос", "Отправить", "Отмена",
		[]*widget.FormItem{
			widget.NewFormItem("Адрес", socket_entry),
			widget.NewFormItem("Запрос", content_entry)},
		func(ok bool) {
			if !ok {
				return
			}

			if socket_entry.Text == "" || content_entry.Text == "" {
				dialog.ShowError(fmt.Errorf("Введите данные перед тем как продолжить"), w)
				return
			}

			err := serviceQuery(socket_entry.Text, content_entry.Text)
			if err != nil {
				dialog.ShowError(err, w)
			}
		}, w)
}
