package main

import (
	"fmt"
	"time"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/dialog"
	"github.com/phpdave11/gofpdf"
)

func createReport(data [][]string, w fyne.Window) error {

	pdf := gofpdf.New("P", "mm", "A4", "./fonts")
	pdf.AddUTF8Font("TimesNewRoman", "", "timesnrcyrmt.ttf")
	pdf.AddUTF8Font("TimesNewRoman", "B", "timesnrcyrmt_bold.ttf")
	pdf.AddPage()

	//header
	pdf.SetFont("TimesNewRoman", "B", 16)
	pdf.Cell(20, 0, "")
	pdf.Cell(40, 5, "Конфигурация сервисов")
	widthsFraction := []float64{0.2, 0.18, 0.13, 0.21, 0.27}

	//header row
	pdf.Ln(10)

	var content [][]rowContents
	for _, row := range data {
		var content_row []rowContents
		for j, str := range row {
			_content := rowContents{Contents: str, ColumnWidthFraction: widthsFraction[j]}
			content_row = append(content_row, _content)
		}
		content = append(content, content_row)
	}
	writeRow(pdf, 4, true, content[0]...)

	//body

	for _, line := range content[1:] {
		writeRow(pdf, 4, false, line...)
	}
	pdf.Ln(-1)

	//Output if no other section available
	if !li.isAdmin {
		SaveToFile(pdf, w)
		return nil
	}

	//header
	pdf.Ln(10)
	pdf.SetFont("TimesNewRoman", "B", 16)
	pdf.Cell(20, 5, "")
	pdf.Cell(40, 5, "Логи")
	widthsFraction = []float64{0.15, 0.3, 0.55}

	selectLogs(&data)

	//header row
	pdf.Ln(10)

	content = [][]rowContents{{}}
	for _, row := range data {
		var content_row []rowContents
		for j, str := range row {
			_content := rowContents{Contents: str, ColumnWidthFraction: widthsFraction[j]}
			content_row = append(content_row, _content)
		}
		content = append(content, content_row)
	}
	writeRow(pdf, 4, true, content[0]...)
	time.Sleep(time.Millisecond * 5)

	//body

	for _, line := range content[1:] {
		writeRow(pdf, 4, false, line...)
	}
	pdf.Ln(-1)

	//Output if no other section available
	if !li.isSecurity {
		SaveToFile(pdf, w)
		return nil
	}
	//header
	pdf.Ln(10)
	pdf.SetFont("TimesNewRoman", "B", 16)
	pdf.Cell(20, 5, "")
	pdf.Cell(40, 5, "Аудит")
	widthsFraction = []float64{0.2, 0.2, 0.2, 0.2, 0.2}

	selectAudit(&data)

	//header row
	pdf.Ln(10)
	pdf.SetFont("TimesNewRoman", "B", 12)

	content = [][]rowContents{{}}
	for _, row := range data {
		var content_row []rowContents
		for j, str := range row {
			_content := rowContents{Contents: str, ColumnWidthFraction: widthsFraction[j]}
			content_row = append(content_row, _content)
		}
		content = append(content, content_row)
	}
	pdf.SetFont("TimesNewRoman", "B", 12)
	writeRow(pdf, 4, true, content[0]...)
	time.Sleep(time.Millisecond * 5)

	//body

	pdf.SetFont("TimesNewRoman", "", 10)
	for _, line := range content[1:] {
		writeRow(pdf, 4, false, line...)
	}
	SaveToFile(pdf, w)
	return nil
}

type rowContents struct {
	Contents            string
	ColumnWidthFraction float64
}

func writeRow(pdf *gofpdf.Fpdf, lineHeight int, header bool, contents ...rowContents) {
	var maxHeight float64

	for _, cellContent := range contents {
		columnWidth := (cellContent.ColumnWidthFraction * 180) - 2 // -2 for table margins

		splitStrings := pdf.SplitText(cellContent.Contents, columnWidth)
		if newHeight := float64(len(splitStrings)) * float64(lineHeight); newHeight > maxHeight {
			maxHeight = newHeight
		}
	}

	y := pdf.GetY()
	for _, cellContent := range contents {
		splitStrings := pdf.SplitText(cellContent.Contents, (cellContent.ColumnWidthFraction*180)-2)
		x := pdf.GetX()
		pdf.CellFormat(cellContent.ColumnWidthFraction*180, maxHeight,
			"", "1", 1, "L", false, 0, "")
		pdf.SetXY(x, y)
		for i, _content := range splitStrings {
			if header {
				pdf.SetFontSize(12)
				pdf.SetFontStyle("B")
				pdf.SetFillColor(240, 240, 240)
			} else {
				pdf.SetFontSize(10)
				pdf.SetFontStyle("")
				pdf.SetFillColor(255, 255, 255)
			}
			if i+1 == len(splitStrings) {
				pdf.CellFormat(cellContent.ColumnWidthFraction*180, maxHeight/float64(len(splitStrings)),
					_content, "0", 0, "L", false, 0, "")
				pdf.SetXY(x+cellContent.ColumnWidthFraction*180, y)
			} else {
				pdf.CellFormat(cellContent.ColumnWidthFraction*180, maxHeight/float64(len(splitStrings)),
					_content, "0", 2, "L", false, 0, "")
				pdf.SetXY(pdf.GetX(), pdf.GetY())
			}
		}
	}
	pdf.SetY(y + maxHeight)
}

func SaveToFile(pdf *gofpdf.Fpdf, w fyne.Window) error {
	dialog.NewFileSave(func(URIwriter fyne.URIWriteCloser, err error) {
		if err != nil {
			dialog.ShowError(err, w)
			return
		}

		if URIwriter == nil {
			dialog.ShowError(fmt.Errorf("No file selected"), w)
			return
		}

		pdf.OutputAndClose(URIwriter)
	}, w).Show()
	return nil
}
