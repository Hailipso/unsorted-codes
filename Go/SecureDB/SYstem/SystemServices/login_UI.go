package main

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
	_ "github.com/lib/pq"
)

func CreateLoginWindow(a fyne.App) fyne.Window {
	w := a.NewWindow("SecureDB Login")

	w.Resize(fyne.NewSize(400, 200))

	w.SetCloseIntercept(func() {
		w.Close()
	})

	label := widget.NewLabel("Введите данные")
	intro_layout := container.New(layout.NewHBoxLayout(),
		layout.NewSpacer(),
		label,
		layout.NewSpacer())

	hostname_label := widget.NewLabel("Адрес БД")
	hostname_entry := widget.NewEntry()
	hostname_entry.SetText(li.hostname)

	login_label := widget.NewLabel("Логин")
	login_entry := widget.NewEntry()
	login_entry.SetText(li.login)

	pass_label := widget.NewLabel("Пароль")
	pass_entry := widget.NewEntry()
	pass_entry.SetText(li.password)

	db_label := widget.NewLabel("имя БД")
	db_entry := widget.NewEntry()
	db_entry.SetText(li.dbname)

	loginInfo_layout := container.New(layout.NewFormLayout(),
		hostname_label, hostname_entry,
		login_label, login_entry,
		pass_label, pass_entry,
		db_label, db_entry)

	login_result_label := widget.NewLabel("")
	login_result_label.TextStyle.Bold = true

	login_bt := widget.NewButton("Login", func() {
		UpdateLoginInfo(li, hostname_entry.Text, login_entry.Text, pass_entry.Text, db_entry.Text, "none")
		var err error
		DB, err = login()
		if err != nil {
			login_result_label.SetText(err.Error())
			return
		}

		err = checkPerms()
		if err != nil {
			login_result_label.SetText(err.Error())
			return
		}

		li.isAuthorized = true
		login_result_label.SetText("Login successful")
		//time.Sleep(0.25 * time.Second)
		w.Close()
	})

	login_result_layout := container.New(layout.NewHBoxLayout(), layout.NewSpacer(), login_result_label, layout.NewSpacer())
	w.SetContent(container.New(
		layout.NewVBoxLayout(),
		intro_layout,
		loginInfo_layout,
		login_bt,
		login_result_layout,
	))

	return w
}
