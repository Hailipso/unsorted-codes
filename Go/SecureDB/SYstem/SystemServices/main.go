// SystemServices project main.go
package main

import (
	"context"

	"fyne.io/fyne/v2/app"
	"go.mongodb.org/mongo-driver/mongo"
)

var li *loginInfo
var DB *mongo.Database

func main() {
	a := app.New()

	li = NewLoginInfo()

	w_work := CreateWorkWindow(a)
	if w_work == nil {
		return
	}
	a.Run()
	if DB != nil {
		DB.Client().Disconnect(context.TODO())
	}
}
