package main

import (
	"context"
	"fmt"
	"regexp"
	"strconv"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

func updatePermissions(serv_socket string, acc_name string, dev_mac string, access string, model string) error {
	if DB == nil {
		return fmt.Errorf("DB connection lost")
	}

	//acc_id
	var answer bson.M
	if err := DB.Collection("Аккаунт").FindOne(context.TODO(), bson.M{"Имя": acc_name}).Decode(&answer); err != nil {
		return err
	}
	acc_id := answer["_id"].(primitive.ObjectID)
	acc_name = answer["Имя"].(string)

	//serv_id
	if err := DB.Collection("Сервис").FindOne(context.TODO(), bson.M{"сокет": serv_socket}).Decode(&answer); err != nil {
		return err
	}
	serv_id := answer["_id"].(primitive.ObjectID)

	//dev_id
	if dev_mac == "" {
		dev_mac = "none"
	} else if dev_mac != "none" {
		r := regexp.MustCompile("([a-fA-F0-9]{2}[:.-]?){5}[a-fA-F0-9]{2}")
		if match := r.Find([]byte(dev_mac)); match == nil {
			return fmt.Errorf("MAC адрес указан неверно")
		}
	}

	filter := bson.M{"Устройство": bson.M{"MAC-адрес": dev_mac}, "Аккаунт": acc_id, "Сервер": serv_id} // TODO: исправить фильтр
	update := bson.M{"$set": bson.M{"Устройство": bson.M{"Доступ": access == "true"}}}
	err := DB.Collection("Разрешения").FindOneAndUpdate(context.TODO(), filter, update).Decode(&answer)
	if err != mongo.ErrNoDocuments {
		if err != nil {
			return err
		}
		err = appendAudit("INFO", "Обновление разрешения", answer["_id"].(primitive.ObjectID))
		return err
	}

	perm := Permission{Service: serv_id, Account: acc_id, Devices: []Device{{MACaddr: dev_mac, Access: access == "Allow", Model: model}}}

	result, err := DB.Collection("Разрешения").InsertOne(context.TODO(), perm)
	if err != nil {
		return err
	}

	err = appendAudit("INFO", fmt.Sprintf("Обновление разрешения: %s для %s", strconv.FormatBool(access == "Allow"), acc_name), result.InsertedID.(primitive.ObjectID))
	return err
}

func appendAudit(severity string, action string, reason primitive.ObjectID) error {
	if DB == nil {
		return fmt.Errorf("DB connection lost")
	}

	audit := auditTable{Account: li.userid, Permission: reason, Severity: severity, Content: action}

	_, err := DB.Collection("Аудит").InsertOne(context.TODO(), audit)

	return err
}

func userUpdate() error {
	if DB == nil {
		return fmt.Errorf("DB connection lost")
	}

	//adminDB := DB.Client().Database("admin")

	cursor, err := DB.Collection("Аккаунт").Find(context.TODO(), bson.M{})
	if err != nil {
		return err
	}

	var accounts []Account
	if err = cursor.All(context.TODO(), &accounts); err != nil {
		return err
	}

	var users bson.M
	if err = DB.RunCommand(context.TODO(), bson.M{
		"usersInfo": 1,
	}).Decode(&users); err != nil {
		return err
	}

exists:
	for _, account := range accounts {
		if account.Name == li.login {
			continue exists
		}
		for _, user := range users["users"].(primitive.A) {
			if account.Name == user.(bson.M)["user"].(string) {
				DB.RunCommand(context.Background(), bson.D{
					{"revokeRolesFromUser", account.Name},
					{"roles", []bson.M{{"role": "_user", "db": "SecureDB"},
						{"role": "_admin", "db": "SecureDB"},
						{"role": "_security", "db": "SecureDB"},
					}},
				})
				DB.RunCommand(context.Background(), bson.D{
					{"grantRolesToUser", account.Name},
					{"roles", []bson.M{{"role": account.Role, "db": "SecureDB"}}},
				})
				continue exists
			}
		}
		r := DB.RunCommand(context.Background(), bson.D{{"createUser", account.Name},
			{"pwd", account.Password}, {"roles", []bson.M{{"role": account.Role, "db": "SecureDB"}}}})
		if r.Err() != nil {
			return r.Err()
		}
	}
	return nil
}
