package main

import (
	"errors"
	"fmt"
	"strings"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type serviceConfiguration struct {
	Oid      primitive.ObjectID `bson:"uID"`
	Socket   string             `bson:"сокет"`
	Protocol string             `bson:"протокол"`
	Name     string             `bson:"имя"`
	Config   string             `bson:"Конфиг"`
}

func (c *serviceConfiguration) toStringArray() []string {
	return []string{c.Name, c.Socket, c.Protocol, c.Config}
}

type adminServiceConfiguration struct {
	ServiceConfig serviceConfiguration `bson:"inline"`
	ReAuthDate    primitive.DateTime   `bson:"Дата_переавторизации"`
}

func (c *adminServiceConfiguration) toStringArray() []string {
	return []string{c.ServiceConfig.Name, c.ServiceConfig.Socket,
		c.ServiceConfig.Protocol,
		strings.Split(c.ReAuthDate.Time().String(), " ")[0],
		c.ServiceConfig.Config}
}

type logsView struct {
	Severity string `bson:"Severity"`
	Name     string `bson:"Сервис"`
	Content  string `bson:"Content,omitempty"`
}

func NewLogsView(name string, content string, severity string) (*logsView, error) {
	if name == "" || severity == "" || content == "" ||
		!(severity == "DEBUG" || severity == "INFO" ||
			severity == "WARNING" || severity == "ERROR" ||
			severity == "FATAL") {
		return nil, fmt.Errorf("wrong arguments provided")
	}

	var c logsView
	c.Name = name
	c.Content = content
	c.Severity = severity
	return &c, nil
}

type LogsTable struct {
	Oid      primitive.ObjectID `bson:"Сервис"`
	Content  string             `bson:"Content"`
	Severity string             `bson:"Severity"`
}

func (c *logsView) toStringArray() []string {
	return []string{c.Severity, c.Name, c.Content}
}

type auditView struct {
	Severity string `bson:"Severity"`
	Account  string `bson:"Аккаунт,omitempty"`
	Service  string `bson:"Сервис,omitempty"`
	Content  string `bson:"Content"`
}

func (c *auditView) toStringArray() []string {
	return []string{c.Severity, c.Account, c.Service, c.Content}
}

type auditTable struct {
	Account    primitive.ObjectID `bson:"Аккаунт"`
	Permission primitive.ObjectID `bson:"Разрешение,omitempty"`
	Severity   string             `bson:"Severity"`
	Content    string             `bson:"Content"`
}

type loginInfo struct {
	login    string
	password string
	hostname string
	dbname   string

	deviceid string
	userid   primitive.ObjectID

	isAuthorized bool
	isAdmin      bool
	isSecurity   bool
}

func NewLoginInfo() *loginInfo {
	var s loginInfo
	s.deviceid = "4C:79:BB:7C:FE:D4"
	s.login = "AccMan"
	s.password = "f32e134b"
	s.hostname = "hailnote:27017"
	s.dbname = "SecureDB"
	s.userid = primitive.NewObjectID()
	s.isAdmin = false
	s.isAuthorized = false
	s.isSecurity = false
	return &s
}
func UpdateLoginInfo(li *loginInfo, hostname string, login string, password string, dbname string, deviceid string) error {
	if li == nil {
		err := errors.New("no login info to update")
		return err
	}
	li.hostname, li.login, li.password, li.dbname = hostname, login, password, dbname
	if deviceid == "" {
		li.deviceid = "1"
	} else {
		li.deviceid = deviceid
	}
	return nil
}

type Account struct {
	Uid      primitive.ObjectID `bson:"_id,omitempty"`
	IsSystem bool               `bson:"Системный"`
	Name     string             `bson:"Имя"`
	Password string             `bson:"Пароль"`
	Role     string             `bson:"Роль"`
	Comment  string             `bson:"Комментарий"`
	User     []User             `bson:"Пользователь"`
}

type User struct {
	FamilyName string `bson:"Фамилия"`
	FirstName  string `bson:"Имя"`
	Thirdname  string `bson:"Отчество"`
	Phone      string `bson:"Номер"`
	Mail       string `bson:"Почта"`
	Other      string `bson:"Прочее"`
}

type Permission struct {
	Service primitive.ObjectID `bson:"Сервис"`
	Account primitive.ObjectID `bson:"Аккаунт"`
	Devices []Device           `bson:"Устройство"`
}

type Device struct {
	Model   string `bson:"модель,omitempty"`
	MACaddr string `bson:"MAC-адрес"`
	Access  bool   `bson:"Доступ"`
}
