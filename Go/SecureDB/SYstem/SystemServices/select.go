package main

import (
	"context"
	"log"

	"go.mongodb.org/mongo-driver/bson"
)

func selectConfiguration(data *[][]string) {
	if DB == nil {
		return
	}

	if !(*li).isAdmin && !(*li).isSecurity {
		var answer []serviceConfiguration

		responseCursor, err := DB.Collection("systemConfig").Find(context.TODO(), bson.D{{
			Key: "uID", Value: li.userid}})
		if err != nil {
			log.Default().Println(err.Error())
			return
		}

		if err = responseCursor.All(context.TODO(), &answer); err != nil {
			log.Default().Println(err.Error())
			return
		}

		*data = [][]string{{"Название сервиса", "Адрес", "Протокол", "Конфигурация"}}

		for _, service := range answer {
			new_data := make([][]string, 0, len(*data)+1)
			new_data = append(new_data, *data...)
			new_data = append(new_data, service.toStringArray())
			*data = new_data
		}
	} else {
		var answer []adminServiceConfiguration

		responseCursor, err := DB.Collection("Сервис").Find(context.TODO(), bson.M{})
		if err != nil {
			log.Default().Println(err.Error())
			return
		}

		if err = responseCursor.All(context.TODO(), &answer); err != nil {
			log.Default().Println(err.Error())
			return
		}

		*data = [][]string{{"Название сервиса", "Адрес", "Протокол", "Дата переавторизации", "Конфигурация"}}

		for _, service := range answer {
			new_data := make([][]string, 0, len(*data)+1)
			new_data = append(new_data, *data...)
			new_data = append(new_data, service.toStringArray())
			*data = new_data
		}
	}
}

func selectLogs(data *[][]string) {
	if DB == nil {
		return
	}

	responseCursor, err := DB.Collection("Logs").Find(context.TODO(), bson.D{{}})
	if err != nil {
		log.Default().Println(err.Error())
		return
	}

	var answer []logsView
	if err = responseCursor.All(context.TODO(), &answer); err != nil {
		log.Default().Println(err.Error())
		return
	}
	*data = [][]string{{"Серьёзность", "Название сервиса", "Сообщение"}}

	for _, log := range answer {
		new_data := make([][]string, 0, len(*data)+1)
		new_data = append(new_data, *data...)
		new_data = append(new_data, log.toStringArray())
		*data = new_data
	}
}

func selectAudit(data *[][]string) {
	if DB == nil {
		return
	}

	responseCursor, err := DB.Collection("Audit").Find(context.TODO(), bson.D{{}})
	if err != nil {
		log.Default().Println(err.Error())
		return
	}

	var answer []auditView
	if err = responseCursor.All(context.TODO(), &answer); err != nil {
		log.Default().Println(err.Error())
		return
	}
	*data = [][]string{{"Серьёзность", "Аккаунт", "Разрешение", "Сообщение"}}

	for _, audit := range answer {
		new_data := make([][]string, 0, len(*data)+1)
		new_data = append(new_data, *data...)
		new_data = append(new_data, audit.toStringArray())
		*data = new_data
	}
}
