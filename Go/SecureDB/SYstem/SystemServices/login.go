package main

import (
	"context"
	"fmt"
	"log"
	"strings"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func login() (result *mongo.Database, err error) {

	client, err := mongo.NewClient(options.Client().ApplyURI(fmt.Sprintf(
		"mongodb://%s:%s@%s:%s/?retryWrites=true&w=majority&authSource=SecureDB",
		li.login,
		li.password,
		strings.FieldsFunc(li.hostname, func(r rune) bool { return strings.ContainsRune(":", r) })[0],
		strings.FieldsFunc(li.hostname, func(r rune) bool { return strings.ContainsRune(":", r) })[1])))
	if err != nil {
		return nil, err
	}

	err = client.Connect(context.TODO())
	if err != nil {
		return nil, err
	}
	err = client.Ping(context.TODO(), nil)
	if err != nil {
		return nil, err
	}

	result = (client.Database(li.dbname))
	if err != nil {
		return nil, err
	}

	var acc_answer Account
	err = result.Collection("Аккаунт").FindOne(context.TODO(), bson.M{
		"Имя": (*li).login,
	}).Decode(&acc_answer)
	if err != nil {
		return nil, err
	}
	li.userid = acc_answer.Uid

	var AM bson.M
	if err = result.Collection("systemConfig").FindOne(context.TODO(), bson.M{
		"сокет": "127.0.0.1",
	}).Decode(&AM); err != nil {
		return nil, fmt.Errorf("wrong device for login or no permission at all")
	}
	AM_oid := AM["_id"].(primitive.ObjectID)

	var perms []bson.M
	response, err := result.Collection("Разрешения").Find(context.TODO(), bson.M{
		"Аккаунт": li.userid,
		"Сервис":  AM_oid,
	})
	if err != nil {
		return nil, fmt.Errorf("wrong device for login or no permission at all")
	}

	response.All(context.TODO(), &perms)

	var perm_oid primitive.ObjectID

	for _, perm := range perms {
		dev_perms := perm["Устройство"].(primitive.A)
		for _, dev_perm := range dev_perms {
			if (dev_perm.(bson.M)["MAC-адрес"].(string) == li.deviceid || dev_perm.(bson.M)["MAC-адрес"].(string) == "none") && (dev_perm.(bson.M)["Доступ"]).(bool) {
				perm_oid = perm["_id"].(primitive.ObjectID)
			}
		}
	}
	//perm_oid := perm["_id"].(primitive.ObjectID)

	audit := auditTable{Account: li.userid, Permission: perm_oid, Severity: "INFO", Content: "Succesful login"}
	_, err = result.Collection("Аудит").InsertOne(context.TODO(), &audit)
	if err != nil {
		return nil, err
	}

	log.Default().Println("Connection established.")

	return result, err
}

func checkPerms() error {
	li.isAuthorized, li.isAdmin, li.isSecurity = false, false, false

	var answer bson.M
	response := DB.RunCommand(context.TODO(), bson.M{"connectionStatus": "1"})

	err := response.Decode(&answer)
	if err != nil {
		return err
	}

	var roles []string
	authInfo := answer["authInfo"].(bson.M)
	userRoles := authInfo["authenticatedUserRoles"].(primitive.A)
	for _, r := range userRoles {
		db_role := r.(bson.M)
		roles = append(roles, db_role["role"].(string))
	}

	for _, role := range roles {
		if role == "_admin" || role == "_security" {
			(*li).isAdmin = true
		}
		if role == "_security" {
			(*li).isSecurity = true
		}
	}

	return nil
}
