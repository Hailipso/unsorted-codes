package main

import (
	"context"
	"log"
	"sync"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/dialog"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

func enterUsers(isSystem bool, name string, pass string, role string, comment string, users *[]User, wg *sync.WaitGroup, w fyne.Window) {
	if DB == nil {
		log.Default().Println("DB connection lost")
		return
	}

	wg.Wait()
	acc := Account{
		IsSystem: isSystem,
		Name:     name,
		Password: pass,
		Role:     role,
		Comment:  comment,
		User:     *users,
	}

	response := DB.Collection("Аккаунт").FindOne(context.TODO(), bson.M{
		"Имя": acc.Name,
	})
	if response.Err() != mongo.ErrNoDocuments {
		dialog.ShowError(response.Err(), w)
		return
	}

	_, err := DB.Collection("Аккаунт").InsertOne(context.TODO(), acc)
	if err != nil {
		dialog.ShowError(err, w)
		return
	}
}
