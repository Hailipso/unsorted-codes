package main

import (
	"context"
	"fmt"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func serviceQuery(socket string, content string) error {

	services := [][]string{{}}
	selectConfiguration(&services)
	if len(services[0]) < 2 {
		return fmt.Errorf("Нет доступных сервисов")
	}

	err := fmt.Errorf("Указанный сервис недоступен")
	for i, j := 1, 1; j < len(services[0]); i++ {
		if socket == services[i][j] {
			err = sendServiceQuery(socket, content)
			break
		}
	}
	if err != nil {
		return err
	}

	return nil
}

func sendServiceQuery(socket string, content string) error {
	if DB == nil {
		return fmt.Errorf("DB is nil")
	}

	var answer bson.M
	if err := DB.Collection("Сервис").FindOne(context.TODO(), bson.M{"сокет": socket}).Decode(&answer); err != nil {
		return err
	}
	oid := answer["_id"].(primitive.ObjectID)

	query := LogsTable{Oid: oid, Content: content, Severity: "INFO"}

	result, err := DB.Collection("Логи").InsertOne(context.TODO(), query)
	if err != nil {
		return err
	}

	fmt.Print(result.InsertedID.(primitive.ObjectID).String())
	return nil
}
