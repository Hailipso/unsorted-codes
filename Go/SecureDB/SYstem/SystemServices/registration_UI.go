package main

import (
	"fmt"
	"strconv"
	"sync"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
)

func CreateRegistrationWindow(a fyne.App) fyne.Window {
	w := a.NewWindow("SecureDB Registration")

	w.Resize(fyne.NewSize(300, 500))

	w.SetCloseIntercept(func() {
		w.Close()
	})

	label := widget.NewLabel("Введите данные")
	intro_layout := container.New(layout.NewHBoxLayout(),
		layout.NewSpacer(),
		label,
		layout.NewSpacer())

	name_label := widget.NewLabel("Имя")
	name_entry := widget.NewEntry()

	pass_label := widget.NewLabel("Пароль")
	pass_entry := widget.NewEntry()

	role_label := widget.NewLabel("Роль")
	role_sl := widget.NewSelect([]string{"_user", "_admin", "_security"}, func(string) {})

	comment_label := widget.NewLabel("Комментарий")
	comment_entry := widget.NewEntry()

	var systemBool bool
	system_label := widget.NewLabel("Системный")
	system_entry := widget.NewSelect([]string{"Нет", "Да"}, func(sel string) {
		if sel == "Да" {
			systemBool = true
		} else {
			systemBool = false
		}
	})

	count_label := widget.NewLabel("Количество пользователей")
	count_entry := widget.NewEntry()

	loginInfo_layout := container.New(layout.NewFormLayout(),
		name_label, name_entry,
		pass_label, pass_entry,
		role_label, role_sl,
		comment_label, comment_entry,
		system_label, system_entry,
		count_label, count_entry)

	login_result_label := widget.NewLabel("")
	login_result_label.TextStyle.Bold = true

	reg_bt := widget.NewButton("Продолжить", func() {
		var users []User

		amount, err := strconv.Atoi(count_entry.Text)
		if err != nil {
			dialog.ShowError(fmt.Errorf("Wrong user count number"), w)
			return
		}

		firstname_entry := widget.NewEntry()
		famname_entry := widget.NewEntry()
		thirdname_entry := widget.NewEntry()
		phone_entry := widget.NewEntry()
		mail_entry := widget.NewEntry()
		else_entry := widget.NewEntry()

		var wg sync.WaitGroup
		wg.Add(amount)
		canceled := false
		for i := 0; i < amount; i++ {
			dialog.ShowForm("Введите пользователя", "Ввести", "Отмена",
				[]*widget.FormItem{
					widget.NewFormItem("Имя", firstname_entry),
					widget.NewFormItem("Фамилия", famname_entry),
					widget.NewFormItem("Отчество", thirdname_entry),
					widget.NewFormItem("Телефон", phone_entry),
					widget.NewFormItem("Почта", mail_entry),
					widget.NewFormItem("Другое", else_entry),
				}, func(ok bool) {
					if !ok {
						canceled = true
						return
					}
					users = append(users, User{
						FamilyName: firstname_entry.Text,
						FirstName:  famname_entry.Text,
						Thirdname:  thirdname_entry.Text,
						Phone:      phone_entry.Text,
						Mail:       mail_entry.Text,
						Other:      else_entry.Text,
					})
					wg.Done()
				}, w)
			if canceled {
				break
			}
		}
		if canceled {
			return
		}

		go enterUsers(systemBool, name_entry.Text, pass_entry.Text, role_sl.Selected, comment_entry.Text, &users, &wg, w)
	})

	w.SetContent(container.New(
		layout.NewVBoxLayout(),
		intro_layout,
		loginInfo_layout,
		reg_bt,
	))

	return w
}
