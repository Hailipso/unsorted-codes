package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"
	"unicode/utf8"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
	"github.com/phpdave11/gofpdf"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var li *loginInfo
var DB *mongo.Database

func serviceQuery(socket string, content string) error {

	services := [][]string{{}}
	selectConfiguration(&services)
	if len(services[0]) < 2 {
		return fmt.Errorf("Нет доступных сервисов")
	}

	err := fmt.Errorf("Указанный сервис недоступен")
	for i, j := 1, 1; j < len(services[0]); i++ {
		if socket == services[i][j] {
			err = sendServiceQuery(socket, content)
			break
		}
	}
	if err != nil {
		return err
	}

	return nil
}

func sendServiceQuery(socket string, content string) error {
	if DB == nil {
		return fmt.Errorf("DB is nil")
	}

	var answer bson.M
	if err := DB.Collection("Сервис").FindOne(context.TODO(), bson.M{"сокет": socket}).Decode(&answer); err != nil {
		return err
	}
	oid := answer["_id"].(primitive.ObjectID)

	query := LogsTable{Oid: oid, Content: content, Severity: "INFO"}

	result, err := DB.Collection("Логи").InsertOne(context.TODO(), query)
	if err != nil {
		return err
	}

	fmt.Print(result.InsertedID.(primitive.ObjectID).String())
	return nil
}

func updatePermissions(serv_socket string, acc_name string, dev_mac string, access string, model string) error {
	if DB == nil {
		return fmt.Errorf("DB connection lost")
	}

	//acc_id
	var answer bson.M
	if err := DB.Collection("Аккаунт").FindOne(context.TODO(), bson.M{"Имя": acc_name}).Decode(&answer); err != nil {
		return err
	}
	acc_id := answer["_id"].(primitive.ObjectID)
	acc_name = answer["Имя"].(string)

	//serv_id
	if err := DB.Collection("Сервис").FindOne(context.TODO(), bson.M{"сокет": serv_socket}).Decode(&answer); err != nil {
		return err
	}
	serv_id := answer["_id"].(primitive.ObjectID)

	//dev_id
	if dev_mac == "" {
		dev_mac = "none"
	} else if dev_mac != "none" {
		r := regexp.MustCompile("([a-fA-F0-9]{2}[:.-]?){5}[a-fA-F0-9]{2}")
		if match := r.Find([]byte(dev_mac)); match == nil {
			return fmt.Errorf("MAC адрес указан неверно")
		}
	}

	filter := bson.M{"Устройство": bson.M{"MAC-адрес": dev_mac}, "Аккаунт": acc_id, "Сервер": serv_id} // TODO: исправить фильтр
	update := bson.M{"$set": bson.M{"Устройство": bson.M{"Доступ": access == "true"}}}
	err := DB.Collection("Разрешения").FindOneAndUpdate(context.TODO(), filter, update).Decode(&answer)
	if err != mongo.ErrNoDocuments {
		if err != nil {
			return err
		}
		err = appendAudit("INFO", "Обновление разрешения", answer["_id"].(primitive.ObjectID))
		return err
	}

	perm := Permission{Service: serv_id, Account: acc_id, Devices: []Device{{MACaddr: dev_mac, Access: access == "Allow", Model: model}}}

	result, err := DB.Collection("Разрешения").InsertOne(context.TODO(), perm)
	if err != nil {
		return err
	}

	err = appendAudit("INFO", fmt.Sprintf("Обновление разрешения: %s для %s", strconv.FormatBool(access == "Allow"), acc_name), result.InsertedID.(primitive.ObjectID))
	return err
}

func appendAudit(severity string, action string, reason primitive.ObjectID) error {
	if DB == nil {
		return fmt.Errorf("DB connection lost")
	}

	audit := auditTable{Account: li.userid, Permission: reason, Severity: severity, Content: action}

	_, err := DB.Collection("Аудит").InsertOne(context.TODO(), audit)

	return err
}

func userUpdate() error {
	if DB == nil {
		return fmt.Errorf("DB connection lost")
	}

	//adminDB := DB.Client().Database("admin")

	cursor, err := DB.Collection("Аккаунт").Find(context.TODO(), bson.M{})
	if err != nil {
		return err
	}

	var accounts []Account
	if err = cursor.All(context.TODO(), &accounts); err != nil {
		return err
	}

	var users bson.M
	if err = DB.RunCommand(context.TODO(), bson.M{
		"usersInfo": 1,
	}).Decode(&users); err != nil {
		return err
	}

exists:
	for _, account := range accounts {
		if account.Name == li.login {
			continue exists
		}
		for _, user := range users["users"].(primitive.A) {
			if account.Name == user.(bson.M)["user"].(string) {
				DB.RunCommand(context.Background(), bson.D{
					{"revokeRolesFromUser", account.Name},
					{"roles", []bson.M{{"role": "_user", "db": "SecureDB"},
						{"role": "_admin", "db": "SecureDB"},
						{"role": "_security", "db": "SecureDB"},
					}},
				})
				DB.RunCommand(context.Background(), bson.D{
					{"grantRolesToUser", account.Name},
					{"roles", []bson.M{{"role": account.Role, "db": "SecureDB"}}},
				})
				continue exists
			}
		}
		r := DB.RunCommand(context.Background(), bson.D{{"createUser", account.Name},
			{"pwd", account.Password}, {"roles", []bson.M{{"role": account.Role, "db": "SecureDB"}}}})
		if r.Err() != nil {
			return r.Err()
		}
	}
	return nil
}

func login() (result *mongo.Database, err error) {

	client, err := mongo.NewClient(options.Client().ApplyURI(fmt.Sprintf(
		"mongodb://%s:%s@%s:%s/?retryWrites=true&w=majority&authSource=SecureDB",
		li.login,
		li.password,
		strings.FieldsFunc(li.hostname, func(r rune) bool { return strings.ContainsRune(":", r) })[0],
		strings.FieldsFunc(li.hostname, func(r rune) bool { return strings.ContainsRune(":", r) })[1])))
	if err != nil {
		return nil, err
	}

	err = client.Connect(context.TODO())
	if err != nil {
		return nil, err
	}
	err = client.Ping(context.TODO(), nil)
	if err != nil {
		return nil, err
	}

	result = (client.Database(li.dbname))
	if err != nil {
		return nil, err
	}

	var acc_answer Account
	err = result.Collection("Аккаунт").FindOne(context.TODO(), bson.M{
		"Имя": (*li).login,
	}).Decode(&acc_answer)
	if err != nil {
		return nil, err
	}
	li.userid = acc_answer.Uid

	var AM bson.M
	if err = result.Collection("systemConfig").FindOne(context.TODO(), bson.M{
		"сокет": "127.0.0.1",
	}).Decode(&AM); err != nil {
		return nil, fmt.Errorf("wrong device for login or no permission at all")
	}
	AM_oid := AM["_id"].(primitive.ObjectID)

	var perms []bson.M
	response, err := result.Collection("Разрешения").Find(context.TODO(), bson.M{
		"Аккаунт": li.userid,
		"Сервис":  AM_oid,
	})
	if err != nil {
		return nil, fmt.Errorf("wrong device for login or no permission at all")
	}

	response.All(context.TODO(), &perms)

	var perm_oid primitive.ObjectID

	for _, perm := range perms {
		dev_perms := perm["Устройство"].(primitive.A)
		for _, dev_perm := range dev_perms {
			if (dev_perm.(bson.M)["MAC-адрес"].(string) == li.deviceid || dev_perm.(bson.M)["MAC-адрес"].(string) == "none") && (dev_perm.(bson.M)["Доступ"]).(bool) {
				perm_oid = perm["_id"].(primitive.ObjectID)
			}
		}
	}
	//perm_oid := perm["_id"].(primitive.ObjectID)

	audit := auditTable{Account: li.userid, Permission: perm_oid, Severity: "INFO", Content: "Succesful login"}
	_, err = result.Collection("Аудит").InsertOne(context.TODO(), &audit)
	if err != nil {
		return nil, err
	}

	log.Default().Println("Connection established.")

	return result, err
}

func checkPerms() error {
	li.isAuthorized, li.isAdmin, li.isSecurity = false, false, false

	var answer bson.M
	response := DB.RunCommand(context.TODO(), bson.M{"connectionStatus": "1"})

	err := response.Decode(&answer)
	if err != nil {
		return err
	}

	var roles []string
	authInfo := answer["authInfo"].(bson.M)
	userRoles := authInfo["authenticatedUserRoles"].(primitive.A)
	for _, r := range userRoles {
		db_role := r.(bson.M)
		roles = append(roles, db_role["role"].(string))
	}

	for _, role := range roles {
		if role == "_admin" || role == "_security" {
			(*li).isAdmin = true
		}
		if role == "_security" {
			(*li).isSecurity = true
		}
	}

	return nil
}

func CreateLoginWindow(a fyne.App) fyne.Window {
	w := a.NewWindow("SecureDB Login")

	w.Resize(fyne.NewSize(400, 200))

	w.SetCloseIntercept(func() {
		w.Close()
	})

	label := widget.NewLabel("Введите данные")
	intro_layout := container.New(layout.NewHBoxLayout(),
		layout.NewSpacer(),
		label,
		layout.NewSpacer())

	hostname_label := widget.NewLabel("Адрес БД")
	hostname_entry := widget.NewEntry()
	hostname_entry.SetText(li.hostname)

	login_label := widget.NewLabel("Логин")
	login_entry := widget.NewEntry()
	login_entry.SetText(li.login)

	pass_label := widget.NewLabel("Пароль")
	pass_entry := widget.NewEntry()
	pass_entry.SetText(li.password)

	db_label := widget.NewLabel("имя БД")
	db_entry := widget.NewEntry()
	db_entry.SetText(li.dbname)

	loginInfo_layout := container.New(layout.NewFormLayout(),
		hostname_label, hostname_entry,
		login_label, login_entry,
		pass_label, pass_entry,
		db_label, db_entry)

	login_result_label := widget.NewLabel("")
	login_result_label.TextStyle.Bold = true

	login_bt := widget.NewButton("Login", func() {
		UpdateLoginInfo(li, hostname_entry.Text, login_entry.Text, pass_entry.Text, db_entry.Text, "none")
		var err error
		DB, err = login()
		if err != nil {
			login_result_label.SetText(err.Error())
			return
		}

		err = checkPerms()
		if err != nil {
			login_result_label.SetText(err.Error())
			return
		}

		li.isAuthorized = true
		login_result_label.SetText("Login successful")
		//time.Sleep(0.25 * time.Second)
		w.Close()
	})

	login_result_layout := container.New(layout.NewHBoxLayout(), layout.NewSpacer(), login_result_label, layout.NewSpacer())
	w.SetContent(container.New(
		layout.NewVBoxLayout(),
		intro_layout,
		loginInfo_layout,
		login_bt,
		login_result_layout,
	))

	return w
}

// SystemServices project main.go

func main() {
	a := app.New()

	li = NewLoginInfo()

	w_work := CreateWorkWindow(a)
	if w_work == nil {
		return
	}
	a.Run()
	if DB != nil {
		DB.Client().Disconnect(context.TODO())
	}
}

func CreateWorkWindow(a fyne.App) fyne.Window {
	w := a.NewWindow("SecureDB")
	w.SetMaster()
	w.Resize(fyne.NewSize(1000, 500))

	var data [][]string = [][]string{{}}
	var _layout *fyne.Container
	var list *widget.Table

	config_bt := fyne.NewMenuItem("Сервисы", func() {
		selectConfiguration(&data)
		updateTableWidth(&list, data)
	})
	config_bt.Disabled = !(li.isAuthorized)

	logs_bt := fyne.NewMenuItem("Логи", func() {
		selectLogs(&data)
		updateTableWidth(&list, data)
	})
	logs_bt.Disabled = !(li.isAdmin || li.isSecurity)

	audit_bt := fyne.NewMenuItem("Аудит", func() {
		selectAudit(&data)
		updateTableWidth(&list, data)
	})
	audit_bt.Disabled = !(li.isSecurity)

	report_bt := fyne.NewMenuItem("Создать отчет", func() {
		err := createReport(data, w)
		if err != nil {
			dialog.ShowError(err, w)
		}
	})
	report_bt.Disabled = !(li.isAuthorized)

	action_bt := fyne.NewMenuItem("Сделать запрос", func() {
		actionIntercept(w)
	})
	action_bt.Disabled = true

	perm_bt := fyne.NewMenuItem("Обновить разрешения", func() { permIntercept(w) })
	perm_bt.Disabled = true

	userUpdate_bt := fyne.NewMenuItem("Обновить пользователей", func() {
		if err := userUpdate(); err != nil {
			dialog.ShowError(err, w)
		} else {
			dialog.ShowInformation("Успешно", "Обновление пользователей успешно завершено", w)
		}
	})
	userUpdate_bt.Disabled = true

	userInsert_bt := fyne.NewMenuItem("Добавить пользователя", func() {
		w_reg := CreateRegistrationWindow(a)
		w_reg.Show()
	})
	userInsert_bt.Disabled = true

	login_win_bt := fyne.NewMenuItem("Авторизация", func() {
		w_login := CreateLoginWindow(a)
		w_login.SetOnClosed(func() {
			selectConfiguration(&data)
			updateTableWidth(&list, data)
			config_bt.Disabled = !(li.isAuthorized)
			report_bt.Disabled = !(li.isAuthorized)
			logs_bt.Disabled = !(li.isAdmin || li.isSecurity)
			action_bt.Disabled = !(li.isAuthorized)
			audit_bt.Disabled = !(li.isSecurity)
			perm_bt.Disabled = !(li.isSecurity)
			userUpdate_bt.Disabled = !(li.isSecurity)
			userInsert_bt.Disabled = !(li.isSecurity)
		})
		w_login.Show()
	})

	menu_login := fyne.NewMenu("Авторизация", login_win_bt)
	menu_tables := fyne.NewMenu("Таблицы",
		config_bt, logs_bt, audit_bt)
	menu_actions := fyne.NewMenu("Действия", action_bt, report_bt)
	menu_adminTools := fyne.NewMenu("Администрирование", perm_bt, userInsert_bt, userUpdate_bt)

	main_menu := fyne.NewMainMenu(menu_login, menu_tables, menu_actions, menu_adminTools)

	list = widget.NewTable(
		func() (int, int) {
			return len(data), len(data[0])
		},
		func() fyne.CanvasObject {

			return widget.NewLabel("very very wide content")
		},
		func(i widget.TableCellID, o fyne.CanvasObject) {
			o.(*widget.Label).SetText(data[i.Row][i.Col])
		})
	updateTableWidth(&list, data)
	list.OnSelected = func(id widget.TableCellID) {
		if id.Row == 0 {
			return
		}
		w.Clipboard().SetContent(data[id.Row][id.Col])
	}

	_layout = container.NewMax(list)

	w.SetMainMenu(main_menu)
	w.SetContent(_layout)
	w.Show()

	return w
}

func updateTableWidth(tab **widget.Table, data [][]string) {
	for i := 0; i < len(data[0]); i++ {
		max_string := ""
		for j := 0; j < len(data); j++ {
			if utf8.RuneCountInString(max_string) < utf8.RuneCountInString(data[j][i]) {
				max_string = data[j][i]
			}
		}
		(*tab).SetColumnWidth(i,
			fyne.MeasureText(max_string, 16, fyne.TextStyle{}).Width+4)
	}
}

func permIntercept(w fyne.Window) {
	acc_entry, serv_entry, dev_entry, model_entry, access_rb := widget.NewEntry(),
		widget.NewEntry(),
		widget.NewEntry(),
		widget.NewEntry(),
		widget.NewRadioGroup([]string{"Allow", "Deny"}, func(p string) {})
	dialog.ShowForm("Ввод разрешения", "Ввести", "Отмена",
		[]*widget.FormItem{
			widget.NewFormItem("Адрес", serv_entry),
			widget.NewFormItem("Аккаунт", acc_entry),
			widget.NewFormItem("Модель", model_entry),
			widget.NewFormItem("MAC-адрес", dev_entry),
			widget.NewFormItem("Доступ", access_rb),
		}, func(ok bool) {
			if !ok {
				return
			}
			err := updatePermissions(serv_entry.Text, acc_entry.Text, dev_entry.Text, access_rb.Selected, model_entry.Text)
			if err != nil {
				dialog.ShowError(err, w)
			}

		}, w)
}

func actionIntercept(w fyne.Window) {
	socket_entry, content_entry := widget.NewEntry(), widget.NewEntry()
	dialog.ShowForm("Запрос", "Отправить", "Отмена",
		[]*widget.FormItem{
			widget.NewFormItem("Адрес", socket_entry),
			widget.NewFormItem("Запрос", content_entry)},
		func(ok bool) {
			if !ok {
				return
			}

			if socket_entry.Text == "" || content_entry.Text == "" {
				dialog.ShowError(fmt.Errorf("Введите данные перед тем как продолжить"), w)
				return
			}

			err := serviceQuery(socket_entry.Text, content_entry.Text)
			if err != nil {
				dialog.ShowError(err, w)
			}
		}, w)
}

func enterUsers(isSystem bool, name string, pass string, role string, comment string, users *[]User, wg *sync.WaitGroup, w fyne.Window) {
	if DB == nil {
		log.Default().Println("DB connection lost")
		return
	}

	wg.Wait()
	acc := Account{
		IsSystem: isSystem,
		Name:     name,
		Password: pass,
		Role:     role,
		Comment:  comment,
		User:     *users,
	}

	response := DB.Collection("Аккаунт").FindOne(context.TODO(), bson.M{
		"Имя": acc.Name,
	})
	if response.Err() != mongo.ErrNoDocuments {
		dialog.ShowError(response.Err(), w)
		return
	}

	_, err := DB.Collection("Аккаунт").InsertOne(context.TODO(), acc)
	if err != nil {
		dialog.ShowError(err, w)
		return
	}
}

func CreateRegistrationWindow(a fyne.App) fyne.Window {
	w := a.NewWindow("SecureDB Registration")

	w.Resize(fyne.NewSize(300, 500))

	w.SetCloseIntercept(func() {
		w.Close()
	})

	label := widget.NewLabel("Введите данные")
	intro_layout := container.New(layout.NewHBoxLayout(),
		layout.NewSpacer(),
		label,
		layout.NewSpacer())

	name_label := widget.NewLabel("Имя")
	name_entry := widget.NewEntry()

	pass_label := widget.NewLabel("Пароль")
	pass_entry := widget.NewEntry()

	role_label := widget.NewLabel("Роль")
	role_sl := widget.NewSelect([]string{"_user", "_admin", "_security"}, func(string) {})

	comment_label := widget.NewLabel("Комментарий")
	comment_entry := widget.NewEntry()

	var systemBool bool
	system_label := widget.NewLabel("Системный")
	system_entry := widget.NewSelect([]string{"Нет", "Да"}, func(sel string) {
		if sel == "Да" {
			systemBool = true
		} else {
			systemBool = false
		}
	})

	count_label := widget.NewLabel("Количество пользователей")
	count_entry := widget.NewEntry()

	loginInfo_layout := container.New(layout.NewFormLayout(),
		name_label, name_entry,
		pass_label, pass_entry,
		role_label, role_sl,
		comment_label, comment_entry,
		system_label, system_entry,
		count_label, count_entry)

	login_result_label := widget.NewLabel("")
	login_result_label.TextStyle.Bold = true

	reg_bt := widget.NewButton("Продолжить", func() {
		var users []User

		amount, err := strconv.Atoi(count_entry.Text)
		if err != nil {
			dialog.ShowError(fmt.Errorf("Wrong user count number"), w)
			return
		}

		firstname_entry := widget.NewEntry()
		famname_entry := widget.NewEntry()
		thirdname_entry := widget.NewEntry()
		phone_entry := widget.NewEntry()
		mail_entry := widget.NewEntry()
		else_entry := widget.NewEntry()

		var wg sync.WaitGroup
		wg.Add(amount)
		canceled := false
		for i := 0; i < amount; i++ {
			dialog.ShowForm("Введите пользователя", "Ввести", "Отмена",
				[]*widget.FormItem{
					widget.NewFormItem("Имя", firstname_entry),
					widget.NewFormItem("Фамилия", famname_entry),
					widget.NewFormItem("Отчество", thirdname_entry),
					widget.NewFormItem("Телефон", phone_entry),
					widget.NewFormItem("Почта", mail_entry),
					widget.NewFormItem("Другое", else_entry),
				}, func(ok bool) {
					if !ok {
						canceled = true
						return
					}
					users = append(users, User{
						FamilyName: firstname_entry.Text,
						FirstName:  famname_entry.Text,
						Thirdname:  thirdname_entry.Text,
						Phone:      phone_entry.Text,
						Mail:       mail_entry.Text,
						Other:      else_entry.Text,
					})
					wg.Done()
				}, w)
			if canceled {
				break
			}
		}
		if canceled {
			return
		}

		go enterUsers(systemBool, name_entry.Text, pass_entry.Text, role_sl.Selected, comment_entry.Text, &users, &wg, w)
	})

	w.SetContent(container.New(
		layout.NewVBoxLayout(),
		intro_layout,
		loginInfo_layout,
		reg_bt,
	))

	return w
}

func createReport(data [][]string, w fyne.Window) error {

	pdf := gofpdf.New("P", "mm", "A4", "./fonts")
	pdf.AddUTF8Font("TimesNewRoman", "", "timesnrcyrmt.ttf")
	pdf.AddUTF8Font("TimesNewRoman", "B", "timesnrcyrmt_bold.ttf")
	pdf.AddPage()

	//header
	pdf.SetFont("TimesNewRoman", "B", 16)
	pdf.Cell(20, 0, "")
	pdf.Cell(40, 5, "Конфигурация сервисов")
	widthsFraction := []float64{0.2, 0.18, 0.13, 0.21, 0.27}

	//header row
	pdf.Ln(10)

	var content [][]rowContents
	for _, row := range data {
		var content_row []rowContents
		for j, str := range row {
			_content := rowContents{Contents: str, ColumnWidthFraction: widthsFraction[j]}
			content_row = append(content_row, _content)
		}
		content = append(content, content_row)
	}
	writeRow(pdf, 4, true, content[0]...)

	//body

	for _, line := range content[1:] {
		writeRow(pdf, 4, false, line...)
	}
	pdf.Ln(-1)

	//Output if no other section available
	if !li.isAdmin {
		SaveToFile(pdf, w)
		return nil
	}

	//header
	pdf.Ln(10)
	pdf.SetFont("TimesNewRoman", "B", 16)
	pdf.Cell(20, 5, "")
	pdf.Cell(40, 5, "Логи")
	widthsFraction = []float64{0.15, 0.3, 0.55}

	selectLogs(&data)

	//header row
	pdf.Ln(10)

	content = [][]rowContents{{}}
	for _, row := range data {
		var content_row []rowContents
		for j, str := range row {
			_content := rowContents{Contents: str, ColumnWidthFraction: widthsFraction[j]}
			content_row = append(content_row, _content)
		}
		content = append(content, content_row)
	}
	writeRow(pdf, 4, true, content[0]...)
	time.Sleep(time.Millisecond * 5)

	//body

	for _, line := range content[1:] {
		writeRow(pdf, 4, false, line...)
	}
	pdf.Ln(-1)

	//Output if no other section available
	if !li.isSecurity {
		SaveToFile(pdf, w)
		return nil
	}
	//header
	pdf.Ln(10)
	pdf.SetFont("TimesNewRoman", "B", 16)
	pdf.Cell(20, 5, "")
	pdf.Cell(40, 5, "Аудит")
	widthsFraction = []float64{0.2, 0.2, 0.2, 0.2, 0.2}

	selectAudit(&data)

	//header row
	pdf.Ln(10)
	pdf.SetFont("TimesNewRoman", "B", 12)

	content = [][]rowContents{{}}
	for _, row := range data {
		var content_row []rowContents
		for j, str := range row {
			_content := rowContents{Contents: str, ColumnWidthFraction: widthsFraction[j]}
			content_row = append(content_row, _content)
		}
		content = append(content, content_row)
	}
	pdf.SetFont("TimesNewRoman", "B", 12)
	writeRow(pdf, 4, true, content[0]...)
	time.Sleep(time.Millisecond * 5)

	//body

	pdf.SetFont("TimesNewRoman", "", 10)
	for _, line := range content[1:] {
		writeRow(pdf, 4, false, line...)
	}
	SaveToFile(pdf, w)
	return nil
}

type rowContents struct {
	Contents            string
	ColumnWidthFraction float64
}

func writeRow(pdf *gofpdf.Fpdf, lineHeight int, header bool, contents ...rowContents) {
	var maxHeight float64

	for _, cellContent := range contents {
		columnWidth := (cellContent.ColumnWidthFraction * 180) - 2 // -2 for table margins

		splitStrings := pdf.SplitText(cellContent.Contents, columnWidth)
		if newHeight := float64(len(splitStrings)) * float64(lineHeight); newHeight > maxHeight {
			maxHeight = newHeight
		}
	}

	y := pdf.GetY()
	for _, cellContent := range contents {
		splitStrings := pdf.SplitText(cellContent.Contents, (cellContent.ColumnWidthFraction*180)-2)
		x := pdf.GetX()
		pdf.CellFormat(cellContent.ColumnWidthFraction*180, maxHeight,
			"", "1", 1, "L", false, 0, "")
		pdf.SetXY(x, y)
		for i, _content := range splitStrings {
			if header {
				pdf.SetFontSize(12)
				pdf.SetFontStyle("B")
				pdf.SetFillColor(240, 240, 240)
			} else {
				pdf.SetFontSize(10)
				pdf.SetFontStyle("")
				pdf.SetFillColor(255, 255, 255)
			}
			if i+1 == len(splitStrings) {
				pdf.CellFormat(cellContent.ColumnWidthFraction*180, maxHeight/float64(len(splitStrings)),
					_content, "0", 0, "L", false, 0, "")
				pdf.SetXY(x+cellContent.ColumnWidthFraction*180, y)
			} else {
				pdf.CellFormat(cellContent.ColumnWidthFraction*180, maxHeight/float64(len(splitStrings)),
					_content, "0", 2, "L", false, 0, "")
				pdf.SetXY(pdf.GetX(), pdf.GetY())
			}
		}
	}
	pdf.SetY(y + maxHeight)
}

func SaveToFile(pdf *gofpdf.Fpdf, w fyne.Window) error {
	dialog.NewFileSave(func(URIwriter fyne.URIWriteCloser, err error) {
		if err != nil {
			dialog.ShowError(err, w)
			return
		}

		if URIwriter == nil {
			dialog.ShowError(fmt.Errorf("No file selected"), w)
			return
		}

		pdf.OutputAndClose(URIwriter)
	}, w).Show()
	return nil
}

func selectConfiguration(data *[][]string) {
	if DB == nil {
		return
	}

	if !(*li).isAdmin && !(*li).isSecurity {
		var answer []serviceConfiguration

		responseCursor, err := DB.Collection("systemConfig").Find(context.TODO(), bson.D{{
			Key: "uID", Value: li.userid}})
		if err != nil {
			log.Default().Println(err.Error())
			return
		}

		if err = responseCursor.All(context.TODO(), &answer); err != nil {
			log.Default().Println(err.Error())
			return
		}

		*data = [][]string{{"Название сервиса", "Адрес", "Протокол", "Конфигурация"}}

		for _, service := range answer {
			new_data := make([][]string, 0, len(*data)+1)
			new_data = append(new_data, *data...)
			new_data = append(new_data, service.toStringArray())
			*data = new_data
		}
	} else {
		var answer []adminServiceConfiguration

		responseCursor, err := DB.Collection("Сервис").Find(context.TODO(), bson.M{})
		if err != nil {
			log.Default().Println(err.Error())
			return
		}

		if err = responseCursor.All(context.TODO(), &answer); err != nil {
			log.Default().Println(err.Error())
			return
		}

		*data = [][]string{{"Название сервиса", "Адрес", "Протокол", "Дата переавторизации", "Конфигурация"}}

		for _, service := range answer {
			new_data := make([][]string, 0, len(*data)+1)
			new_data = append(new_data, *data...)
			new_data = append(new_data, service.toStringArray())
			*data = new_data
		}
	}
}

func selectLogs(data *[][]string) {
	if DB == nil {
		return
	}

	responseCursor, err := DB.Collection("Logs").Find(context.TODO(), bson.D{{}})
	if err != nil {
		log.Default().Println(err.Error())
		return
	}

	var answer []logsView
	if err = responseCursor.All(context.TODO(), &answer); err != nil {
		log.Default().Println(err.Error())
		return
	}
	*data = [][]string{{"Серьёзность", "Название сервиса", "Сообщение"}}

	for _, log := range answer {
		new_data := make([][]string, 0, len(*data)+1)
		new_data = append(new_data, *data...)
		new_data = append(new_data, log.toStringArray())
		*data = new_data
	}
}

func selectAudit(data *[][]string) {
	if DB == nil {
		return
	}

	responseCursor, err := DB.Collection("Audit").Find(context.TODO(), bson.D{{}})
	if err != nil {
		log.Default().Println(err.Error())
		return
	}

	var answer []auditView
	if err = responseCursor.All(context.TODO(), &answer); err != nil {
		log.Default().Println(err.Error())
		return
	}
	*data = [][]string{{"Серьёзность", "Аккаунт", "Разрешение", "Сообщение"}}

	for _, audit := range answer {
		new_data := make([][]string, 0, len(*data)+1)
		new_data = append(new_data, *data...)
		new_data = append(new_data, audit.toStringArray())
		*data = new_data
	}
}

type serviceConfiguration struct {
	Oid      primitive.ObjectID `bson:"uID"`
	Socket   string             `bson:"сокет"`
	Protocol string             `bson:"протокол"`
	Name     string             `bson:"имя"`
	Config   string             `bson:"Конфиг"`
}

func (c *serviceConfiguration) toStringArray() []string {
	return []string{c.Name, c.Socket, c.Protocol, c.Config}
}

type adminServiceConfiguration struct {
	ServiceConfig serviceConfiguration `bson:"inline"`
	ReAuthDate    primitive.DateTime   `bson:"Дата_переавторизации"`
}

func (c *adminServiceConfiguration) toStringArray() []string {
	return []string{c.ServiceConfig.Name, c.ServiceConfig.Socket,
		c.ServiceConfig.Protocol,
		strings.Split(c.ReAuthDate.Time().String(), " ")[0],
		c.ServiceConfig.Config}
}

type logsView struct {
	Severity string `bson:"Severity"`
	Name     string `bson:"Сервис"`
	Content  string `bson:"Content,omitempty"`
}

func NewLogsView(name string, content string, severity string) (*logsView, error) {
	if name == "" || severity == "" || content == "" ||
		!(severity == "DEBUG" || severity == "INFO" ||
			severity == "WARNING" || severity == "ERROR" ||
			severity == "FATAL") {
		return nil, fmt.Errorf("wrong arguments provided")
	}

	var c logsView
	c.Name = name
	c.Content = content
	c.Severity = severity
	return &c, nil
}

type LogsTable struct {
	Oid      primitive.ObjectID `bson:"Сервис"`
	Content  string             `bson:"Content"`
	Severity string             `bson:"Severity"`
}

func (c *logsView) toStringArray() []string {
	return []string{c.Severity, c.Name, c.Content}
}

type auditView struct {
	Severity string `bson:"Severity"`
	Account  string `bson:"Аккаунт,omitempty"`
	Service  string `bson:"Сервис,omitempty"`
	Content  string `bson:"Content"`
}

func (c *auditView) toStringArray() []string {
	return []string{c.Severity, c.Account, c.Service, c.Content}
}

type auditTable struct {
	Account    primitive.ObjectID `bson:"Аккаунт"`
	Permission primitive.ObjectID `bson:"Разрешение,omitempty"`
	Severity   string             `bson:"Severity"`
	Content    string             `bson:"Content"`
}

type loginInfo struct {
	login    string
	password string
	hostname string
	dbname   string

	deviceid string
	userid   primitive.ObjectID

	isAuthorized bool
	isAdmin      bool
	isSecurity   bool
}

func NewLoginInfo() *loginInfo {
	var s loginInfo
	s.deviceid = "4C:79:BB:7C:FE:D4"
	s.login = ""
	s.password = ""
	s.hostname = "localhost:27017"
	s.dbname = "SecureDB"
	s.userid = primitive.NewObjectID()
	s.isAdmin = false
	s.isAuthorized = false
	s.isSecurity = false
	return &s
}
func UpdateLoginInfo(li *loginInfo, hostname string, login string, password string, dbname string, deviceid string) error {
	if li == nil {
		err := errors.New("no login info to update")
		return err
	}
	li.hostname, li.login, li.password, li.dbname = hostname, login, password, dbname
	if deviceid == "" {
		li.deviceid = "1"
	} else {
		li.deviceid = deviceid
	}
	return nil
}

type Account struct {
	Uid      primitive.ObjectID `bson:"_id,omitempty"`
	IsSystem bool               `bson:"Системный"`
	Name     string             `bson:"Имя"`
	Password string             `bson:"Пароль"`
	Role     string             `bson:"Роль"`
	Comment  string             `bson:"Комментарий"`
	User     []User             `bson:"Пользователь"`
}

type User struct {
	FamilyName string `bson:"Фамилия"`
	FirstName  string `bson:"Имя"`
	Thirdname  string `bson:"Отчество"`
	Phone      string `bson:"Номер"`
	Mail       string `bson:"Почта"`
	Other      string `bson:"Прочее"`
}

type Permission struct {
	Service primitive.ObjectID `bson:"Сервис"`
	Account primitive.ObjectID `bson:"Аккаунт"`
	Devices []Device           `bson:"Устройство"`
}

type Device struct {
	Model   string `bson:"модель,omitempty"`
	MACaddr string `bson:"MAC-адрес"`
	Access  bool   `bson:"Доступ"`
}
