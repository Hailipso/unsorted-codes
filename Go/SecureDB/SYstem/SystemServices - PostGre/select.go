package main

import (
	"database/sql"

	"fyne.io/fyne/v2/widget"
)

func selectConfiguration(DB *sql.DB, data *[][]string, list *widget.Table) {
	if DB != nil {

		answer, err := DB.Query("SELECT * FROM SERVICECONFIGURATION")
		if err != nil {
			*data = [][]string{{err.Error()}}
			return
		}
		defer answer.Close()

		*data = [][]string{{"Название сервиса", "Адрес", "Протокол", "Конфигурация"}}

		for answer.Next() {
			var config serviceConfiguration
			if err := answer.Scan(&config.name, &config.socket, &config.protocol, &config.config); err != nil {
				break
			}
			new_data := make([][]string, 0, len(*data)+1)
			new_data = append(new_data, *data...)
			new_data = append(new_data, config.toStringArray())
			*data = new_data
		}
	}
	updateTableWidth(&list, *data)
}

func selectLogs(DB *sql.DB, data *[][]string, list *widget.Table) {
	if DB != nil {

		answer, err := DB.Query("SELECT * FROM LOGS_VIEW")
		if err != nil {
			*data = [][]string{{err.Error()}}
			return
		}
		defer answer.Close()

		*data = [][]string{{"Серьезность", "Название сервиса", "Сообщение"}}

		for answer.Next() {
			var logs logsView
			if err := answer.Scan(&logs.severity, &logs.name, &logs.content); err != nil {
				break
			}
			new_data := make([][]string, 0, len(*data)+1)
			new_data = append(new_data, *data...)
			new_data = append(new_data, logs.toStringArray())
			*data = new_data
		}
	}
	updateTableWidth(&list, *data)
}

func selectAudit(DB *sql.DB, data *[][]string, list *widget.Table) {
	if DB != nil {

		answer, err := DB.Query("SELECT * FROM AUDIT_VIEW")
		if err != nil {
			*data = [][]string{{err.Error()}}
			return
		}
		defer answer.Close()

		*data = [][]string{{"Серьезность", "Аккаунт пользователя", "Номер разрешения", "Сообщение"}}

		for answer.Next() {
			var audit auditView
			if err := answer.Scan(&audit.severity, &audit.account, &audit.permission, &audit.content); err != nil {
				break
			}
			new_data := make([][]string, 0, len(*data)+1)
			new_data = append(new_data, *data...)
			new_data = append(new_data, audit.toStringArray())
			*data = new_data
		}
	}
	updateTableWidth(&list, *data)
}
