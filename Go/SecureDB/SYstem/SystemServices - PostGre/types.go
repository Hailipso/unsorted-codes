package main

import "errors"

type serviceConfiguration struct {
	name     string
	socket   string
	protocol string
	config   string
}

func (c *serviceConfiguration) toStringArray() []string {
	return []string{c.name, c.socket, c.protocol, c.config}
}

type logsView struct {
	severity string
	name     string
	content  string
}

func (c *logsView) toStringArray() []string {
	return []string{c.severity, c.name, c.content}
}

type auditView struct {
	severity   string
	account    string
	permission string
	content    string
}

func (c *auditView) toStringArray() []string {
	return []string{c.severity, c.account, c.permission, c.content}
}

type loginInfo struct {
	login          string
	password       string
	noone_password string
	hostname       string
	dbname         string
	deviceid       string
	isAuthorized   bool
	isAdmin        bool
	isSecurity     bool
}

func NewLoginInfo() *loginInfo {
	var s loginInfo
	s.deviceid = "1"
	s.login = "AccMan"
	s.password = "f32e134b"
	s.noone_password = "ac31f351"
	s.hostname = "hailnote:27017"
	s.dbname = "SecureDB"
	s.isAdmin = false
	s.isAuthorized = false
	s.isSecurity = false
	return &s
}

func UpdateLoginInfo(li *loginInfo, hostname string, login string, password string, dbname string, noone string, deviceid string) error {
	if li == nil {
		err := errors.New("no login info to update")
		return err
	}
	li.hostname, li.login, li.password, li.dbname, li.noone_password = hostname, login, password, dbname, noone
	if deviceid == "" {
		li.deviceid = "1"
	} else {
		li.deviceid = deviceid
	}
	return nil
}
