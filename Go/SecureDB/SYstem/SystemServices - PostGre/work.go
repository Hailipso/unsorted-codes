package main

import (
	"database/sql"
	"image/color"

	_ "github.com/lib/pq"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"

	"github.com/phpdave11/gofpdf"
)

func CreateWorkWindow(a fyne.App, li *loginInfo, DB *sql.DB) fyne.Window {
	w := a.NewWindow("SecureDB")
	w.SetMaster()
	w.Resize(fyne.NewSize(800, 500))

	var data [][]string = [][]string{{}}
	var _layout *fyne.Container
	var list *widget.Table

	config_bt := widget.NewButton("Сервисы", func() {
		selectConfiguration(DB, &data, list)
	})
	config_bt.Hidden = !(li.isAuthorized)

	logs_bt := widget.NewButton("Логи", func() {
		selectLogs(DB, &data, list)
	})
	logs_bt.Hidden = !(li.isAdmin || li.isSecurity)

	audit_bt := widget.NewButton("Аудит", func() {
		selectAudit(DB, &data, list)
	})
	audit_bt.Hidden = !(li.isSecurity)

	report_bt := widget.NewButton("Создать отчет", func() {
		err := createReport(data)
		if err != nil {
			panic(err.Error())
		}
	})
	report_bt.Hidden = !(li.isAuthorized)

	login_win_bt := widget.NewButton("Авторизация", func() {
		w_login := CreateLoginWindow(a, li, &DB)
		w_login.SetOnClosed(func() {
			selectConfiguration(DB, &data, list)
			config_bt.Hidden = !(li.isAuthorized)
			report_bt.Hidden = !(li.isAuthorized)
			logs_bt.Hidden = !(li.isAdmin || li.isSecurity)
			audit_bt.Hidden = !(li.isSecurity)
		})
		w_login.Show()
	})

	menu :=
		container.New(layout.NewHBoxLayout(),
			login_win_bt,
			config_bt,
			logs_bt,
			audit_bt,
			layout.NewSpacer(),
			report_bt)

	list = widget.NewTable(
		func() (int, int) {
			return len(data), len(data[0])
		},
		func() fyne.CanvasObject {

			return widget.NewLabel("very very wide content")
		},
		func(i widget.TableCellID, o fyne.CanvasObject) {
			o.(*widget.Label).SetText(data[i.Row][i.Col])
		})
	updateTableWidth(&list, data)

	_layout = container.NewBorder(menu, nil, nil, nil, list)
	w.SetContent(_layout)
	w.Show()

	//TODO: implement sql Query
	//TODO: select query on account perms
	//TODO: implement report creation

	return w
}

func updateTableWidth(tab **widget.Table, data [][]string) {
	for i := 0; i < len(data[0]); i++ {
		max_string := ""
		for j := 0; j < len(data); j++ {
			if len(max_string) < len(data[j][i]) {
				max_string = data[j][i]
			}
		}
		(*tab).SetColumnWidth(i,
			float32(len(max_string)*int(canvas.NewText("", color.White).TextSize))/1.8)
	}
}

func createReport(data [][]string) error {

	pdf := gofpdf.New("P", "mm", "A4", "./fonts/")
	pdf.SetCompression(false)
	pdf.AddPage()
	//pdf.AddFont("Helvetica", "", "helvetica_1251.json")
	pdf.SetFont("Helvetica", "B", 16)
	tr := pdf.UnicodeTranslatorFromDescriptor("")
	pdf.Cell(20, 0, "")
	pdf.Cell(40, 10, tr("Конфигурация сервисов"))
	pdf.Ln(20)
	//err := pdf.OutputFileAndClose("./output_test.pdf")
	//if err != nil {
	//	return err
	//}

	widths := getColumnWidths(data, 180, pdf)

	pdf.SetFont("Times", "B", 14)
	pdf.SetFillColor(240, 240, 240)
	for i, str := range data[0] {
		pdf.CellFormat(widths[i], 7, tr(str), "1", 0, "L", true, 0, "")
	}
	pdf.Ln(-1)
	// err := pdf.OutputFileAndClose("./output_test2.pdf")
	// if err != nil {
	// 	return err
	// }

	pdf.SetFont("Times", "", 14)
	pdf.SetFillColor(255, 255, 255)

	for _, line := range data[1:] {
		for j, str := range line {
			pdf.CellFormat(widths[j], 7, tr(str), "1", 0, "L", false, 0, "")
		}
		pdf.Ln(-1)
	}

	err := pdf.OutputFileAndClose("output.pdf")
	return err
}

func getColumnWidths(data [][]string, total int, pdf *gofpdf.Fpdf) []float64 {
	var widths []float64
	for i := 0; i < len(data[0]); i++ {
		max_string := ""
		for j := 0; j < len(data); j++ {
			if len(max_string) < len(data[j][i]) {
				max_string = data[j][i]
			}
		}
		widths = append(widths, pdf.GetStringWidth(max_string))
	}
	sum := 0.0
	for _, v := range widths {
		sum += v
	}
	for i, col := range widths {
		col = col / sum * float64(total)
		widths[i] = col
	}
	return widths
}
