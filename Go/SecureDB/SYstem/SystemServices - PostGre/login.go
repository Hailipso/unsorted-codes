package main

import (
	"database/sql"
	"fmt"
	"strings"
	"time"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
	_ "github.com/lib/pq"
)

func CreateLoginWindow(a fyne.App, li *loginInfo, DB **sql.DB) fyne.Window {
	w := a.NewWindow("SecureDB Login")

	w.Resize(fyne.NewSize(400, 200))

	w.SetCloseIntercept(func() {
		w.Close()
	})

	label := widget.NewLabel("Введите данные")
	intro_layout := container.New(layout.NewHBoxLayout(),
		layout.NewSpacer(),
		label,
		layout.NewSpacer())

	device_label := widget.NewLabel("Код устройства")
	device_entry := widget.NewEntry()
	device_entry.SetText(li.deviceid)

	hostname_label := widget.NewLabel("Адрес БД")
	hostname_entry := widget.NewEntry()
	hostname_entry.SetText(li.hostname)

	login_label := widget.NewLabel("Логин")
	login_entry := widget.NewEntry()
	login_entry.SetText(li.login)

	pass_label := widget.NewLabel("Пароль")
	pass_entry := widget.NewEntry()

	db_label := widget.NewLabel("имя БД")
	db_entry := widget.NewEntry()
	db_entry.SetText(li.dbname)

	noone_label := widget.NewLabel("Пароль подключения")
	noone_entry := widget.NewEntry()

	loginInfo_layout := container.New(layout.NewFormLayout(),
		device_label, device_entry,
		hostname_label, hostname_entry,
		noone_label, noone_entry,
		login_label, login_entry,
		pass_label, pass_entry,
		db_label, db_entry)

	login_result_label := widget.NewLabel("")
	login_result_label.TextStyle.Bold = true

	login_bt := widget.NewButton("Login", func() {
		UpdateLoginInfo(li, hostname_entry.Text, login_entry.Text, pass_entry.Text, db_entry.Text, noone_entry.Text, device_entry.Text)
		var err error
		*DB, err = login(li)
		if err != nil {
			login_result_label.SetText(err.Error())
			return
		}

		err = checkPerms(&li, *DB)
		if err != nil {
			login_result_label.SetText(err.Error())
			return
		}

		li.isAuthorized = true
		login_result_label.SetText("Login successful")
		time.Sleep(1 * time.Second)
		w.Close()
	})

	login_result_layout := container.New(layout.NewHBoxLayout(), layout.NewSpacer(), login_result_label, layout.NewSpacer())
	w.SetContent(container.New(
		layout.NewVBoxLayout(),
		intro_layout,
		loginInfo_layout,
		login_bt,
		login_result_layout,
	))

	return w
}

func login(li *loginInfo) (result *sql.DB, err error) {
	psqlInfo := fmt.Sprintf("host=%s port=%s user=noone password=%s dbname=%s sslmode=disable",
		strings.FieldsFunc(li.hostname, func(r rune) bool { return strings.ContainsRune(":", r) })[0],
		strings.FieldsFunc(li.hostname, func(r rune) bool { return strings.ContainsRune(":", r) })[1],
		li.noone_password, li.dbname)

	result, err = sql.Open("postgres", psqlInfo)
	if err != nil {
		return result, err
	}
	fmt.Printf("Connection established\n")

	err = result.Ping()
	if err != nil {
		return result, err
	}
	fmt.Printf("Noone login succesful\n")

	_, err = result.Exec("CALL login($1, $2, $3)", li.login, li.password, li.deviceid)
	return result, err
}

func checkPerms(li **loginInfo, DB *sql.DB) error {
	(*li).isAuthorized = false
	(*li).isAdmin = false
	(*li).isSecurity = false
	answer, err := DB.Query("SELECT * FROM PARENT_ROLES")
	if err != nil {
		return err
	}
	defer answer.Close()

	for answer.Next() {
		var role string
		if err := answer.Scan(&role); err != nil {
			break
		}
		(*li).isSecurity = (*li).isSecurity || (role == "_security")
		(*li).isAdmin = (*li).isAdmin || (role == "_admin")
	}
	return nil
}
