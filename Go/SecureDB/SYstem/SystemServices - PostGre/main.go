// SystemServices project main.go
package main

import (
	"database/sql"

	"fyne.io/fyne/v2/app"
)

func main() {
	a := app.New()
	var DB *sql.DB
	li := NewLoginInfo()

	w_work := CreateWorkWindow(a, li, DB)
	if w_work == nil {
		return
	}
	a.Run()
}
