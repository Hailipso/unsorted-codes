package main

import "fmt"

func main() {
	a := make([]int, 3, 4)
	b := []int{1}
	a = append(a, b...)
	fmt.Printf("%v, Length: %v, Capacity: %v\n", a, len(a), cap(a))
	//	fmt.Printf("%v, %v", b, cap(b))
}
